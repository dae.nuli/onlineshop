<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/','RentalKamera@index');

Route::get('/blog/{slug?}','RentalKamera@blog');


Route::get('order','RentalKamera@order');
Route::post('order','RentalKamera@postOrder');

Route::get('how','RentalKamera@how');
Route::get('promo/{slug?}','RentalKamera@promo');

Route::get('contact','RentalKamera@contact');
Route::post('contact','RentalKamera@postContact');

Route::get('search','RentalKamera@search');

Route::get('404','RentalKamera@not');


Route::controller('login','HomeController');
Route::group(array('before'=>'auth_admin'),function(){

	Route::group(array('prefix' => 'admin'), function(){

		Route::get('/',function(){
			return Redirect::to('/admin/home');
		});
		Route::get('/home','Dashboard@index');
		Route::post('/home','Dashboard@postIndex');
		Route::controller('/profile','Profile');
		//ACL
		Route::group(array('before' => 'acl'), function(){

			Route::controller('/product_category','ProductsCategory');
			Route::controller('/products','Products');
			
			Route::controller('/news_category','NewsCategory');
			Route::controller('/news','News');
			

			Route::controller('/about','About');
			Route::controller('/testimony','Testimony');
			Route::controller('/how','How');
			Route::controller('/banner','Banner');
			Route::controller('/statistics','Statistics');
			Route::controller('/report','Report');
			Route::controller('/messages','Messages');
			
		});
		Route::group(array('before' => 'acl_admin'), function(){

			Route::controller('/users','Users');
			Route::controller('/group','Groups');
			Route::controller('/permission','Permissions');

		});
	});
	Route::get('/logout','Dashboard@logout');
	
});
// App::error(function($exception, $code) { 
// 	if(Request::is('admin/*')){
// 		switch ($code) {
// 			case 404:
// 				$errors = 'Page not found';
// 				$note   = 'We could not find the page you were looking for';
// 				break;
// 			default:
// 				$errors = 'Something went wrong';
// 				$note   = 'We will work on fixing that right away';
// 				break;
// 		}
// 		View::share('title',$code);
// 		View::share('path',$errors);
// 		$data['code']  = $code;
// 		$data['error'] = $errors;
// 		$data['note']  = $note;
// 		return View::make('backend.errors',$data);
// 	}
// });

// App::error(function($exception, $code) { 
// 	if(!Request::is('admin/*')){		
// 		return View::make('front.404');
// 	}
// });
Route::get('{category?}/{slug?}','RentalKamera@category');
