@extends('eshopper.layouts.content')


@section('body-content')
<div class="features_items"><!--features_items-->
	<h2 class="title text-center">Promo</h2>
	<div class="col-sm-12">
		<p style="text-align: center;"><img src="{{asset($promo->picture)}}" class="img-responsive" alt="{{$promo->title}}" style="display: inline;" /></p>
		<p><center><b>{{$promo->title}}</b></center></p>
		{{$promo->content}}
	</div>
</div><!--features_items-->

@stop