@extends('eshopper.layouts.content')

@section('advertisement')
	<section id="advertisement">
		<div class="container">
			<img src="{{asset('eshopper/images/shop/advertisement.jpg')}}" alt="" />
		</div>
	</section>
@stop

@section('body-content')
<div class="features_items"><!--features_items-->
	<h2 class="title text-center">Products</h2>
	@foreach($product as $row)
	<div class="col-sm-4">
		<div class="product-image-wrapper">
			<div class="single-products">
					<div class="productinfo text-center">
						<a href="{{URL::to(Eshopper::URLbyCode($row->id))}}" data-toggle="tooltip" data-placement="top" title="Click to view detail"><img src="{{asset($row->picture)}}" alt="" /></a>
						<h2>{{number_format($row->price,0,",",".")}}</h2>
						<p><a href="{{URL::to(Eshopper::URLbyCode($row->id))}}" class="code_link" data-toggle="tooltip" data-placement="top" title="Click to view detail">{{$row->name}}</a></p>
						<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
					</div>
			</div>
		</div>
	</div>
	@endforeach
	{{$product->links()}}
</div><!--features_items-->
@stop

@section('footer-script')
	@parent
	<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
    </script>
@stop