@extends('eshopper.layouts.content')

@section('body-content')
<div class="product-details"><!--product-details-->
	<div class="col-sm-5">
		<div class="view-product">
			<img src="{{asset($detail->picture)}}" alt="" />
			<h3>{{$detail->categories->name}}</h3>
		</div>
		{{-- <div id="similar-product" class="carousel slide" data-ride="carousel">
			
			  <!-- Wrapper for slides -->
			    <div class="carousel-inner">
					<div class="item active">
					  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
					</div>
					<div class="item">
					  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
					</div>
					<div class="item">
					  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
					  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
					</div>
					
				</div>

			  <!-- Controls -->
			  <a class="left item-control" href="#similar-product" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			  </a>
			  <a class="right item-control" href="#similar-product" data-slide="next">
				<i class="fa fa-angle-right"></i>
			  </a>
		</div> --}}

	</div>
	<div class="col-sm-7">
		<div class="product-information"><!--/product-information-->
			<h2>{{$detail->name}}</h2>
			<p><b>Rental price:</b> {{number_format($detail->price,0,",",".")}}</p>
			<p><b>Availability:</b> In Stock</p>
			<p><b>Condition:</b> {{$detail->note}}</p>
		</div><!--/product-information-->
	</div>
</div><!--/product-details-->

<div class="recommended_items"><!--recommended_items-->
	<h2 class="title text-center">Other Products</h2>
	
	<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			{{Eshopper::OtherProducts($detail->id,$detail->id_category)}}
		</div>
		 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
			<i class="fa fa-angle-left"></i>
		  </a>
		  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
			<i class="fa fa-angle-right"></i>
		  </a>			
	</div>
</div><!--/recommended_items-->
@stop

@section('footer-script')
	@parent
	<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
    </script>
@stop