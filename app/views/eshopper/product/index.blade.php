@extends('eshopper.layouts.content')
{{--
@section('advertisement')
	<section id="advertisement">
		<div class="container">
			<img src="{{asset('eshopper/images/shop/advertisement.jpg')}}" alt="" />
		</div>
	</section>
@stop
--}}
@section('body-content')
<div class="features_items"><!--features_items-->
	<h2 class="title text-center">{{$category->name}}</h2>
	@foreach($product as $row)
	<div class="col-sm-3">
		<div class="product-image-wrapper">
			<div class="single-products">
					<div class="productinfo text-center">
						<a href="{{URL::to(Eshopper::URLbyCode($row->id))}}"><img src="{{asset($row->picture)}}" alt="" /></a>
					</div>
			</div>
			<p><a href="{{URL::to(Eshopper::URLbyCode($row->id))}}" class="code_link">{{$row->name}}</a></p>
			<h4>Rp. {{number_format($row->price,0,",",".")}}</h4>
		</div>
	</div>
	@endforeach
	<div class="col-sm-12">
		{{$product->links()}}
	</div>
</div><!--features_items-->
@stop

@section('footer-script')
	@parent
	<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
    </script>
@stop