@extends('eshopper.layouts.content')

@section('body-content')
<div class="blog-post-area">
	<h2 class="title text-center">Latest From our Blog</h2>
	@foreach($blog as $row)
	<div class="single-blog-post">
		<h3>{{$row->title}}</h3>
		<div class="post-meta">
			<ul>
				<li><i class="fa fa-user"></i> {{$row->author->name}}</li>
				<li><i class="fa fa-clock-o"></i> {{date('g:i a',strtotime($row->created_at))}}</li>
				<li><i class="fa fa-calendar"></i> {{date('M j, Y',strtotime($row->created_at))}}DEC 5, 2013</li>
			</ul>
			<span> {{FrontEnd::views($row->id,2)}} views </span>
		</div>
		<a href="{{URL::to('blog/'.$row->slug)}}">
			<img src="{{asset($row->picture)}}" alt="">
		</a>
		<p>{{Str::words($row->content,50, ' ...')}}</p>
		<a  class="btn btn-primary" href="{{URL::to('blog/'.$row->slug)}}">Read More</a>
	</div>
	@endforeach
	<div class="pagination-area">
		{{$blog->links()}}
	</div>
</div>
@stop
