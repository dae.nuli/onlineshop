@extends('eshopper.layouts.base')

@section('head-script')
    <link href="{{asset('eshopper/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('eshopper/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('eshopper/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('eshopper/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('eshopper/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('eshopper/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('eshopper/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
@stop

@section('header')
<?php $about = Eshopper::about(); ?>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> {{$about->phone}}</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> {{$about->email}}</a></li>
								<li><a href="#"><span class="bbm-icon"></span> {{$about->bbm}}</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="https://facebook.com/{{$about->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/{{$about->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://instagram.com/{{$about->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
								<li><a href="https://plus.google.com/u/0/{{$about->gplus}}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="{{URL::to('/')}}"><img src="{{asset('eshopper/images/logo.png')}}" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href="{{URL::to('/')}}"><i class="fa fa-home"></i> Home</a></li>
								<li><a href="{{URL::to('how')}}"><i class="fa fa-info-circle"></i> How to buy</a></li>
								<li><a href="{{URL::to('contact')}}"><i class="fa fa-envelope"></i> Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<?php $uri1 = Request::segment(1); ?>
						<?php $menu = Helper::CategoryMenu(); ?>
						<div class="mainmenu pull-left bg-color">
							<ul class="nav navbar-nav collapse navbar-collapse">
								@foreach($menu as $row)
									<li><a href="{{URL::to($row->slug)}}" @if($uri1 == $row->slug) class="active" @endif>{{$row->name}}</a></li>
								@endforeach
{{-- 								<li><a href="{{URL::to('video')}}" @if($uri1 == 'video') class="active" @endif>Video</a></li>
								<li><a href="{{URL::to('events')}}" @if($uri1 == 'events') class="active" @endif>Events</a></li>
								<li><a href="{{URL::to('how')}}" @if($uri1 == 'how') class="active" @endif>How to order</a></li>
								<li><a href="{{URL::to('blog')}}" @if($uri1 == 'blog') class="active" @endif>Blog</a></li>
								<li><a href="{{URL::to('contact')}}" @if($uri1 == 'contact') class="active" @endif>Contact</a></li> --}}
							</ul>
						</div>
					</div>
					{{-- <div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div> --}}
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
@stop

{{-- @yield('contact-body') --}}

@section('body')
	@yield('advertisement')
	<section>
		<div class="container">
			<div class="row">
				{{-- <div class="col-sm-12"> --}}
					@yield('body-content')
				{{-- </div> --}}
			</div>
		</div>
	</section>
@stop

@section('footer')
	
	<footer id="footer">
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					{{-- <p class="pull-left">Copyright &copy; 2015 {{$about->name}}. All rights reserved.</p> --}}
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
@stop

@section('footer-script')
    <script src="{{asset('eshopper/js/jquery.js')}}"></script>
	<script src="{{asset('eshopper/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('eshopper/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('eshopper/js/price-range.js')}}"></script>
    <script src="{{asset('eshopper/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('eshopper/js/main.js')}}"></script>
@stop