<?php $about = Eshopper::about(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{$about->name}} | {{$title}}</title>
    @yield('head-script')
</head><!--/head-->

<body>
  	@yield('header')

  	@yield('banner')

  	@yield('body')

  	@yield('footer')
  
  	@yield('footer-script')
</body>
</html>