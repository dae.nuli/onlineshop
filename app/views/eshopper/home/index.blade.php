@extends('eshopper.layouts.content')

@section('banner')

<section id="slider"><!--slider-->
		<div class="container">
	<h2 class="title text-center">Promo</h2>
			<div class="row">
				@foreach($banner as $row)
					<div class="col-sm-4 pr-10 ban">
						<div class="wrapper-pamflet">
							<a href="{{URL::to('promo/'.$row->slug)}}"><img src="{{asset($row->picture)}}" class="img-responsive" style="position: relative; top:{{$row->position}}px"></a>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</section><!--/slider-->
@stop

@section('body-content')
<div class="features_items"><!--features_items-->
	<h2 class="title text-center">New Product</h2>
	@foreach($displayProduct as $row)
	<div class="col-sm-3 pro-3">
		<div class="product-image-wrapper">
			<div class="single-products">
					<div class="productinfo text-center">
						<a href="{{URL::to(Eshopper::URLbyCode($row->id))}}"><img src="{{asset($row->picture)}}" alt="" /></a>
					</div>
			</div>
			<p><a href="{{URL::to(Eshopper::URLbyCode($row->id))}}" class="code_link">{{$row->name}}</a></p>
			<h4>Rp. {{number_format($row->price,0,",",".")}}</h4>
		</div>
	</div>
	@endforeach
	
</div><!--features_items-->

{{-- <div class="recommended_items"><!--recommended_items-->
	<h2 class="title text-center">Best Seller</h2>
	
	<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			{{Eshopper::BestSeller()}}
		</div>
		 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
			<i class="fa fa-angle-left"></i>
		  </a>
		  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
			<i class="fa fa-angle-right"></i>
		  </a>			
	</div>
</div> --}}<!--/recommended_items-->
@stop

@section('footer-script')
	@parent
	<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
    </script>
@stop