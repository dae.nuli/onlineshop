@extends('eshopper.layouts.content')


@section('body-content')
<div class="features_items"><!--features_items-->
	<h2 class="title text-center">How to Buy</h2>
	<div class="col-sm-12">
		{{$how->content}}
	</div>
</div><!--features_items-->

@stop

@section('footer-script')
	@parent
	<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
    </script>
@stop