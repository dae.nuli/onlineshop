@extends('eshopper.layouts.content')
	 
@section('body-content')
<div id="contact-page">  		
		<h2 class="title text-center">Contact <strong>Us</strong></h2>    			    				    				
		<div class="col-sm-8">    			   			
			<div id="map" class="contact-map">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="contact-info">
				<h2 class="title text-center">Contact Info</h2>
				<address>
					<p>{{$description->name}}</p>
					<table>
						<tr>
							<td><b>Mobile</b></td>
							<td>{{$description->phone}}</td>
						</tr>
						<tr>
							<td><b>BBM</b></td>
							<td>{{$description->bbm}}</td>
						</tr>
						<tr>
							<td><b>Email</b></td>
							<td>{{$description->email}}</td>
						</tr>
						<tr>
							<td><b>Address</b></td>
							<td>{{$description->address}}</td>
						</tr>
					</table>
				</address>
				<div class="social-networks">
					<h2 class="title text-center">Social Networking</h2>
					<ul>
						<li>
							<a href="https://facebook.com/{{$description->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
						</li>
						<li>
							<a href="https://twitter.com/{{$description->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
						</li>
						<li>
							<a href="https://plus.google.com/u/0/{{$description->gplus}}" target="_blank"><i class="fa fa-google-plus"></i></a>
						</li>
						<li>
							<a href="https://instagram.com/{{$description->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
</div><!--/#contact-page-->
@stop

@section('footer-script')
	@parent
	
	<script>
	function initMap() {
	  var uluru = {lat: {{$description->lat}}, lng: {{$description->lng}}};
	  var map = new google.maps.Map(document.getElementById('map'), {
	    zoom: 8,
	    center: uluru
	  });

	  var contentString = '{{$description->name}}';

	  var infowindow = new google.maps.InfoWindow({
	    content: contentString
	  });

	  var marker = new google.maps.Marker({
	    position: uluru,
	    map: map,
	    title: '{{$description->name}}'
	  });
	  marker.addListener('click', function() {
	    infowindow.open(map, marker);
	  });
	}
    </script>
	<script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>
    {{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&sensor=false"></script>
    <script type="text/javascript" src="{{asset('eshopper/js/gmaps.js')}}"></script>
	<script src="{{asset('eshopper/js/contact.js')}}"></script>--}}
@stop