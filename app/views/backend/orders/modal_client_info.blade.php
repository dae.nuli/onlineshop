
<script type="text/javascript">
$(function() {
    $('#compose-modal').modal('show');
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Client Info</h4>
            </div>
            <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Client Name</label>
                            <input type="text" value="{{$client->name}}" readonly class="form-control" id="name" placeholder="Client Name">
                        </div>
                        <div class="form-group hides">
                            <label for="email">Email Address</label>
                            <input type="email" value="{{$client->email}}" readonly class="form-control" id="email" placeholder="Email Address">
                        </div>
                        <div class="form-group hides">
                            <label for="phone">Phone Number</label>
                            <input type="text" value="{{$client->phone}}" readonly class="form-control" id="phone" placeholder="Phone Number">
                        </div>
                        <div class="form-group hides">
                            <label for="address">Address</label>
                            <textarea class="form-control" readonly id="address" rows="3" placeholder="Address">{{$client->address}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
        </div>
    </div>
</div>