<script src="{{asset('assets/js/jquery.autocomplete.js')}}"></script>
<link href="{{asset('assets/css/autocomplete.min.css')}}" rel="stylesheet" />
<script type="text/javascript">
$(function() {
// var countries = [
// @foreach($names as $row)
//    { value: '{{$row->name}}', data: '{{$row->id}}' },
// @endforeach
// ];

// $('.autocomplete').autocomplete({
//     lookup: countries,
//     lookupLimit:10,
//     onSelect: function (hasil) {
//         $('.id_client').val(hasil.data);
//         $('.hides').hide();
//     }
// });
// $(document).on('input','.autocomplete',function(){
//     $('.id_client').val(null);
//     $('.hides').show();
// });

// $(document).on('input','.exist_email',function(){
//     var emails = $(this).val();
//     $.ajax({
//         url : '/admin/orders/exist-email',
//         type : 'POST',
//         data : {email:emails},
//         success : function(data){
//             if(data==1){
//                 $('.spa').html('<p class="text-red">The email has already been taken.</p>');
//             }else{
//                 $('.spa').html(null);
//             }
//         }
//     });
// });

$('#compose-modal').modal('show');

});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Order</h4>
            </div>
            <form action="{{URL::to('admin/orders/add-order')}}" method="POST">
                <input type="hidden" name="id_client" class="id_client" value="">
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Client Name</label>
                            <input type="text" name="name" autocomplete="off" class="form-control autocomplete" id="name" placeholder="Client Name">
                        </div>
                        <div class="form-group hides">
                            <label for="email">Email Address</label>
                            <input type="email" name="email" autocomplete="off" class="form-control exist_email" id="email" placeholder="Email Address">
                            <span class="spa"></span>
                        </div>
                        <div class="form-group hides">
                            <label for="phone">Phone Number</label>
                            <input type="text" name="phone" autocomplete="off" class="form-control" id="phone" placeholder="Phone Number">
                        </div>
                        <div class="form-group hides">
                            <label for="address">Address</label>
                            <textarea class="form-control" name="address" id="address" rows="3" placeholder="Address"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>