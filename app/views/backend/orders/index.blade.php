@extends('backend.layouts.content')

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a class="btn btn-primary add-orders">Create</a>
</div>
@stop

@section('body-content')
@if(Session::has('orders'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('orders')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover images">
            <thead>
            <tr>
                <th>Order ID</th>
                <th>Client Name</th>
                <th>Total</th>
                <th>Payment Status</th>
                <th>Order Status</th>
                <th>Order Date</th>
                <th>Finish Create</th>
                <th>Action</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($orders as $row)
            <tr>
                <td><a href="{{URL::to('admin/orders/detail/'.$row->id)}}">{{$row->code_order}}</a></td>
                <td><a href="#" data-idclient="{{$row->id_client}}" class="info-client">{{$row->client->name}}</a></td>
                <td>Rp {{number_format($row->total,0,",",".")}}</td>
                <td>{{Helper::CheckPayment($row->id)}}</td>
                <td>{{Helper::CheckOrder($row->id)}}</td>
                <td>{{date('d F Y',strtotime($row->created_at))}}</td>
                <td>
                    @if(strtotime($row->finish_create))
                        {{date('d F Y',strtotime($row->finish_create))}}
                    @else
                        -
                    @endif
                </td>
                <td>
                    <a href="{{URL::to('admin/orders/print/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-print"></i> Print</a>
                    <a href="{{URL::to('admin/orders/delete-order/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$orders->links()}}
    </div>
</div>
<div id="open-modal"></div>
@stop