<link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
<script src="{{asset('assets/css/select2.js')}}"></script>
<script type="text/javascript">
$(function() {
        // $(document).ready(function() {
    $('.selectpro').select2({
        theme: "classic"
    });
    $('#compose-modal').modal('show');
    $(document).on('change','.products',function(){
        var code = $(this).val();
        $.ajax({
            type : 'POST',
            url  : "/admin/orders/find-price-by-product",
            data : {id_product:code}
        }).done(function(data){
            $('.prices').val(data);
        });
    })
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Item</h4>
            </div>
            <form action="{{URL::to('admin/orders/add-item/'.$orders)}}" method="POST">
                <div class="modal-body">
                    <div class="box-body">  
                        <div class="form-group">
                            <label for="product">Products</label>
                            <select class="form-control products selectpro" style="padding:0;" id="product" name="product">
                                    <option value="">- Select Product -</option>
                                @foreach($products as $row)
                                    <option value="{{$row->id}}">{{$row->code}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" name="price" class="form-control prices" value="" id="price" placeholder="Price Product" readonly>
                        </div>
                        <div class="form-group">
                            <label for="qty">Qty</label>
                            <input type="text" name="qty" class="form-control" id="qty" placeholder="Qty Product">
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>