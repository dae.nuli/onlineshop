@extends('backend.layouts.content')

@section('end-script')
    @parent

    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
    <script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
    <script type="text/javascript">
    jQuery('.datetimepicker2').datetimepicker({
        closeOnDateSelect:true,
        format:'Y-m-d',
        timepicker:false,
        scrollInput:false
    });
    jQuery('.timepicker2').datetimepicker({
        format:'H:i',
        datepicker:false,
        scrollInput:false
    });

    </script>
@stop
 

@section('body-content-child')
@if(Session::has('errors'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('errors')}}.
    </div>
@endif
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('success')}}.
    </div>
@endif

{{Form::open(array('url'=>'admin/orders/data/'.((isset($data->id))?$data->id:''), 'method'=>'POST'))}}
<input type="hidden" name="orders_detail_id" value="{{(isset($detail->id))?$detail->id:''}}">
<input type="hidden" name="id_orders" value="{{(isset($detail->id_order))?$detail->id_order:''}}">
<div class="col-xs-12">
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Data Mempelai</h3>
        </div>
    </div><!-- /.box -->
</div>
<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-3">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-body">
                    <h4>Pihak Wanita</h4>
                    <div class="form-group">
                        <label for="a">Nama Mempelai Wanita</label>
                        <input type="text" name="nama_mempelai_wanita" value="{{(isset($data->woman_name))?$data->woman_name:''}}" class="form-control" id="a" placeholder="Nama Mempelai Wanita">
                    </div>
                    <div class="form-group">
                        <label for="b">Nama Ayah</label>
                        <input type="text" name="nama_ayah_mempelai_wanita" value="{{(isset($data->woman_father))?$data->woman_father:''}}" class="form-control" id="b" placeholder="Nama Ayah Mempelai Wanita"> 
                    </div>
                    <div class="form-group">
                        <label for="c">Nama Ibu</label>
                        <input type="text" name="nama_ibu_mempelai_wanita" value="{{(isset($data->woman_mother))?$data->woman_mother:''}}" class="form-control" id="c" placeholder="Nama Ibu Mempelai Wanita">
                    </div>
                    <div class="form-group">
                        <label for="d">Alamat</label>
                        <textarea class="form-control" rows="3" name="alamat_mempelai_wanita">{{(isset($data->woman_address))?$data->woman_address:''}}</textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-xs-3">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-body">
                    <h4>Pihak Pria</h4>
                    <div class="form-group">
                        <label for="a">Nama Mempelai Pria</label>
                        <input type="text" name="nama_mempelai_pria" value="{{(isset($data->man_name))?$data->man_name:''}}" class="form-control" id="a" placeholder="Nama Mempelai Pria">
                    </div>
                    <div class="form-group">
                        <label for="b">Nama Ayah</label>
                        <input type="text" name="nama_ayah_mempelai_pria" value="{{(isset($data->man_father))?$data->man_father:''}}" class="form-control" id="b" placeholder="Nama Ayah Mempelai Pria"> 
                    </div>
                    <div class="form-group">
                        <label for="c">Nama Ibu</label>
                        <input type="text" name="nama_ibu_mempelai_pria" value="{{(isset($data->man_mother))?$data->man_mother:''}}" class="form-control" id="c" placeholder="Nama Ibu Mempelai Pria">
                    </div>
                    <div class="form-group">
                        <label for="d">Alamat</label>
                        <textarea class="form-control" rows="3" name="alamat_mempelai_pria">{{(isset($data->man_address))?$data->man_address:''}}</textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-xs-3">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-body">
                    <h4>Akad Nikah</h4>
                    <div class="form-group">
                        <label>Hari, tanggal</label>
                        <input type="text" name="akad_tanggal" autocomplete="off" value="{{(isset($data->akad_date))?$data->akad_date:''}}" class="form-control datetimepicker2" id="" placeholder="{{date('Y-m-d')}}">
                    </div>
                    <div class="form-group">
                        <label>Jam</label>
                        <input type="text" name="akad_jam" autocomplete="off" value="{{(isset($data->akad_time))?date('H:i',strtotime($data->akad_time)):''}}" class="form-control timepicker2" id="" placeholder="{{date('H:i')}}">
                    </div>
                    <div class="form-group">
                        <label>Lokasi</label>
                        <input type="text" name="akad_lokasi" value="{{(isset($data->akad_location))?$data->akad_location:''}}" class="form-control" id="" placeholder="Lokasi Akad">
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-xs-3">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-body">
                    <h4>Resepsi</h4>
                    <div class="form-group">
                        <label>Hari, tanggal</label>
                        <input type="text" name="resepsi_tanggal" autocomplete="off" value="{{(isset($data->resepsi_date))?$data->resepsi_date:''}}" class="form-control datetimepicker2" id="" placeholder="{{date('Y-m-d')}}">
                    </div>
                    <div class="form-group">
                        <label>Jam</label>
                        <input type="text" name="resepsi_jam" autocomplete="off" value="{{(isset($data->resepsi_time))?date('H:i',strtotime($data->resepsi_time)):''}}" class="form-control timepicker2" id="" placeholder="{{date('H:i')}}">
                    </div>
                    <div class="form-group">
                        <label>Lokasi</label>
                        <input type="text" name="resepsi_lokasi" value="{{(isset($data->resepsi_location))?$data->resepsi_location:''}}" class="form-control" id="" placeholder="Lokasi Resepsi">
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</div>
<div class="col-xs-6">
    <!-- general form elements -->
    <div class="box box-success">
        <div class="box-body">
            <div class="form-group">
                <label>Nama Panggilan Wanita</label>
                <input type="text" name="panggilan_wanita" value="{{(isset($data->nickname_woman))?$data->nickname_woman:''}}" class="form-control" id="" placeholder="Nama Panggilan Wanita">
            </div>
            <div class="form-group">
                <label>Nama Panggilan Pria</label>
                <input type="text" name="panggilan_pria" value="{{(isset($data->nickname_man))?$data->nickname_man:''}}" class="form-control" id="" placeholder="Nama Panggilan Pria">   
            </div>
            <div class="form-group">
                <label>Agama</label>
                <select class="form-control" name="agama">
                    <?php $rel = (isset($data->id_religion))?$data->id_religion:0; ?>
                    @foreach($religion as $row)
                        @if($row->id == $rel)
                            <option value="{{$row->id}}" selected>{{$row->name}}</option>
                        @else
                            <option value="{{$row->id}}">{{$row->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Turut Mengundang</label>
                <textarea class="form-control" rows="3" name="turut_mengundang">{{(isset($data->important_invitation))?$data->important_invitation:''}}</textarea>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box no-border" style="padding:10px">
        <a href="{{URL::to('admin/orders/detail/'.$detail->id_order)}}" class="btn btn-default">{{trans('button.bc')}}</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{{Form::close()}}
@stop