<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example 2</title>
    <link rel="stylesheet" href="{{asset('/front/pdf.css')}}" media="all" />
  </head>
  <body>
<?php $arini = FrontEnd::about(); ?>
    <header class="clearfix">
      <div id="logo">
        <img src="{{asset('front/arini.png')}}">
      </div>
      <div id="company">
        <h2 class="name">{{$arini->name}}</h2>
        <div>{{$arini->address}}</div>
        <div>{{$arini->phone}}</div>
        <div><a href="mailto:{{$arini->email}}">{{$arini->email}}</a></div>
      </div>
    </header><br><br><br><br><br><br>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">INVOICE TO:</div>
          <h2 class="name">{{$client->name}}</h2>
          <div class="address">{{$client->address}}</div>
          <div class="phone">{{$client->phone}}</div>
          <div class="email"><a href="mailto:{{$client->email}}">{{$client->email}}</a></div>
        </div>
        <div id="invoice">
          <h1>INVOICE : {{$orders->code_order}}</h1>
          <div class="date">Date : {{date('d F Y')}}</div>
        </div>
      </div><br><br><br><br><br><br><br>
      <label><h1>ITEMS</h1></label>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">DESCRIPTION</th>
            <th class="unit">UNIT PRICE</th>
            <th class="qty">QUANTITY</th>
            <th class="total">TOTAL</th>
          </tr>
        </thead>
        <tbody>
          @if(count($detail)>0)
            <?php $nomor=1; ?>
            @foreach($detail as $row)
            <?php $subtotal = ($row->qty*$row->item_price); ?>
            <tr>
              <td class="no">0{{$nomor++}}</td>
              <td class="desc"><h3>{{Helper::FindCategory($row->code_product)}}</h3>{{Helper::FindSubCategory($row->code_product)}}, {{$row->code_product}}</td>
              <td class="unit">Rp {{number_format($row->item_price,0,",",".")}}</td>
              <td class="qty">{{$row->qty}}</td>
              <td class="total">Rp {{number_format($subtotal,0,",",".")}}</td>
            </tr>
            @endforeach
          @else
            <tr><td colspan="5"><center>Data is empty</center></td></tr>
          @endif
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">GRAND TOTAL</td>
            <td>Rp {{number_format($orders->total,0,",",".")}}</td>
          </tr>
        </tfoot>
      </table>
      <label><h1>PAYMENT HISTORY</h1></label>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">DATE</th>
            <th class="unit">PAYMENT METHOD</th>
            <th class="qty">AMOUNT</th>
            <th class="total">NOTE</th>
          </tr>
        </thead>
        <tbody>
          @if(count($payments)>0)
            <?php $nomor=1; ?>
            @foreach($payments as $row)
            <tr>
              <td class="no">0{{$nomor++}}</td>
              <td class="desc">{{date("d F Y",strtotime($row->payment_date))}}</td>
              <td class="unit payment">{{$row->method->name}}</td>
              <td class="qty">Rp {{number_format($row->amount,0,",",".")}}</td>
              <td class="total">{{$row->note}}</td>
            </tr>
            <?php $Am[] = $row->amount; ?>
            <?php $payTot = array_sum($Am); ?>
            @endforeach
          @else
            <tr><td colspan="5"><center>Data is empty</center></td></tr>
          @endif
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td>GRAND TOTAL</td>
            <td>Rp @if(!empty($payTot)) {{number_format($payTot,0,",",".")}} @else 0 @endif</td>
            <td>{{Helper::CheckPaymentStatus($orders->id)}}</td>
          </tr>
        </tfoot>
      </table>

      <div id="thanks">Thank you!</div>
      <div id="notices">
        <div>Note:</div>
        <div class="notice">{{$orders->note}}</div>
      </div>
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>