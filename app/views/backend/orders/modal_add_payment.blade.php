
<script src="{{asset('assets/js/accounting.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
<script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
<script type="text/javascript">
jQuery('.datetimepicker2').datetimepicker({
    closeOnDateSelect:true,
    format:'Y-m-d',
    timepicker:false,
    scrollInput:false
});

$(function() {
    $('#compose-modal').modal('show');
    var amount = $('.amount').val();
    $('.amount').val(accounting.formatNumber(amount));

    $(document).on('input','.amount',function(){
        var amount = $(this).val();
        $('.amount').val(accounting.formatNumber(amount));
    });
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Payment</h4>
            </div>
            <form action="{{URL::to('admin/orders/add-payment/'.$orders)}}" method="POST">
                <div class="modal-body">
                    <div class="box-body">  
                        <div class="form-group">
                            <label for="method">Payment Method</label>
                            <select class="form-control" id="method" name="method">
                                    <option value="">- Select Method -</option>
                                @foreach($method as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input type="text" name="amount" autocomplete="off" class="form-control amount" value="{{($amount)?$amount:''}}" id="amount" placeholder="Amount">
                        </div>
                        <div class="form-group">
                            <label for="date">Payment Date</label>
                            <input type="text" name="date" autocomplete="off" class="form-control datetimepicker2" id="date" placeholder="Payment Date">
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea class="form-control" name="note" id="note" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>