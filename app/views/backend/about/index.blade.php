@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script type="text/javascript" src="{{ asset('fileman/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('fileman/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function openCustomRoxy(){
          $('#roxyCustomPanel').dialog({modal:true, width:875,height:600});
        }
        function closeCustomRoxy(){
          $('#roxyCustomPanel').dialog('close');
        }        
    </script>

    <script type="text/javascript" src="{{asset('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        // tinymce.init({
        //     selector: "textarea",
        //     plugins: [
        //         ["advlist autolink link image lists charmap hr anchor pagebreak spellchecker"],
        //         ["searchreplace wordcount insertdatetime media nonbreaking"],
        //         ["save table contextmenu directionality emoticons template paste"]
        //     ], 
        //     schema: "html5", 
        //     toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ",
        //     style_formats:[
        //         {title:'Left Margin',selector:'img',styles:{'margin-left':'5px'}},
        //         {title:'Right Margin',selector:'img',styles:{'margin-right':'5px'}},
        //         {title:'Top Margin',selector:'img',styles:{'margin-right':'5px'}},
        //         {title:'Bottom Margin',selector:'img',styles:{'margin-right':'5px'}}
        //     ],
        //     relative_urls: false,
        //     // file_browser_callback: RoxyFileBrowser
        //  });

        // function RoxyFileBrowser(field_name, url, type, win) {
        //   var roxyFileman = '/fileman/index.html?integration=tinymce4';
        //   if (roxyFileman.indexOf("?") < 0) {     
        //     roxyFileman += "?type=" + type;   
        //   }
        //   else {
        //     roxyFileman += "&type=" + type;
        //   }
        //   roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
        //   if(tinyMCE.activeEditor.settings.language){
        //     roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
        //   }
        //   tinyMCE.activeEditor.windowManager.open({
        //      file: roxyFileman,
        //      title: 'Roxy Fileman',
        //      width: 850, 
        //      height: 650,
        //      resizable: "yes",
        //      plugins: "media",
        //      inline: "yes",
        //      close_previous: "no"  
        //   }, { window: win, input: field_name });
        //   return false; 
        // }

    });
    </script>
    <link href="{{asset('assets/css/gmaps.css')}}" rel="stylesheet">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&sensor=false"></script>
    <script>
      var map;
      var myCenter = new google.maps.LatLng({{$about->lat}},{{$about->lng}});
      var geocoder;
      var markers = [];
      // var tempat;
      function initialize()
      {
        var markers = [];
        var mapProp = {
            center:myCenter,
            zoom:9,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };

        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
       
        var input = document.getElementById('pac-input');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var searchBox = new google.maps.places.SearchBox(input);
        
        google.maps.event.addListener(searchBox, 'places_changed', function() {
            var places = searchBox.getPlaces();
      
            for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
            }
        
        markers = [];
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = places[i]; i++) {
            var image = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };
            var marker = new google.maps.Marker({
                map: map,
                icon: image,
                title: place.name,
                center:place.geometry.location,
                position: place.geometry.location
            });
      
            markers.push(marker);
      
            bounds.extend(place.geometry.location);
        }
      
            map.fitBounds(bounds);
        });
        
        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });
        google.maps.event.addListener(map, 'bounds_changed', function() {
            var bounds = map.getBounds();
            searchBox.setBounds(bounds);
        });
      }

        function placeMarker(location) {
            setAllMap(null);
        
        var marker = new google.maps.Marker({
            position: location,
            center: location,
            map: map,
        });
        markers.push(marker);

        getAddress(location);

        var infowindow = new google.maps.InfoWindow({
            content: 'Longitude: ' + location.lng().toFixed(6) + '<br>Latitude: ' + location.lat().toFixed(6) + '<br>Place: <span class="palace"></span><br><a class="btn btn-primary btn-xs latlng" data-descfrom="" data-long="'+location.lng().toFixed(6)+'" data-lat="'+location.lat().toFixed(6)+'">Get it</a>'
        });
        infowindow.open(map,marker);
      }
      function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
        markers = [];
      }
      function getAddress(latLng) {
        // var tempat;
        geocoder.geocode( {'latLng': latLng},
          function(results, status) {
            if(status == google.maps.GeocoderStatus.OK) {
              if(results[0]) {
                $('.palace').html(results[0].formatted_address);
              }
            }
          });

        }
    google.maps.event.addDomListener(window, 'load', initialize);

        $(document).ready(function() {
            $(document).on('click','.latlng',function(){
              $('.longitude').val($(this).data('long'));
              $('.latitude').val($(this).data('lat'));
            });
        });
    </script>
@stop

@section('body-content')
@if(Session::has('about'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('about')}}.
    </div>
@endif
<div class="box">
    {{Form::open(array('url'=>'admin/about', 'method'=>'POST', 'files'=>true))}}
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{$about->name}}" class="form-control" id="name" placeholder="Name">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-4">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{$about->email}}" class="form-control" id="email" placeholder="Email Address">
                        {{$errors->first('email','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-4">
                        <label for="facebook">Facebook</label>
                        <input type="text" name="facebook" value="{{$about->facebook}}" class="form-control" id="facebook" placeholder="Facebook Account">
                        <p class="help-block"><del>https://facebook.com/</del><b><u>FacebookOnlineShopAnda</u></b></p>
                        {{$errors->first('facebook','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <label for="twitter">Twitter</label>
                        <input type="text" name="twitter" value="{{$about->twitter}}" class="form-control" id="twitter" placeholder="Twitter Account">
                        <p class="help-block"><del>https://twitter.com/</del><b><u>TwitterOnlineShopAnda</u></b></p>
                        {{$errors->first('twitter','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-4">
                        <label for="instagram">Instagram</label>
                        <input type="text" name="instagram" value="{{$about->instagram}}" class="form-control" id="instagram" placeholder="Instagram Account">
                        <p class="help-block"><del>https://instagram.com/</del><b><u>InstagramOnlineShopAnda</u></b></p>
                        {{$errors->first('instagram','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-4">
                        <label for="gplus">Google Plus</label>
                        <input type="text" name="gplus" value="{{$about->gplus}}" class="form-control" id="gplus" placeholder="Google plus account">
                        <p class="help-block"><del>https://plus.google.com/u/0/</del><b><u>104894969154772273309</u></b></p>
                        {{$errors->first('gplus','<p class="text-red">:message</p>')}}
                    </div>
                    
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <label for="bbm">Bbm</label>
                        <input type="text" name="bbm" value="{{$about->bbm}}" class="form-control" id="bbm" placeholder="Pin Bbm">
                        {{$errors->first('bbm','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-4">
                        <label for="phone">Phone Number</label>
                        <input type="text" name="phone_number" value="{{$about->phone}}" class="form-control" id="phone" placeholder="Phone Number">
                        {{$errors->first('phone_number','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-4">
                        <label for="address">Address</label>
                        <input type="text" name="address" value="{{$about->address}}" class="form-control" id="address" placeholder="Address">
                        {{$errors->first('address','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Latitude</label>
                            <input type="text" value="{{$about->lat}}" class="form-control latitude" placeholder="Latitude" disabled>

                            <input type="hidden" name="latitude" class="latitude" value="{{$about->lat}}"/>
                            {{$errors->first('latitude','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Longitude</label>
                            <input type="text" value="{{$about->lng}}" class="form-control longitude" placeholder="Longitude" disabled>
                            <input type="hidden" name="longitude" class="longitude" value="{{$about->lng}}"/>
                            {{$errors->first('longitude','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="body">Web Keywords</label>
                        <textarea id="body" name="web_keywords" class="form-control textarea" style="height:250px" rows="3" placeholder="Web Keywords">{{$about->web_keywords}}</textarea>
                        {{$errors->first('web_keywords','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label>Google Maps</label>
                        <input id="pac-input" class="controls sub" type="text" placeholder="Search Place">
                        <div id="googleMap" style="height:250px;"></div>
                    </div>
                </div>
            </div>
            {{-- <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="home">Home Description</label>
                        <input type="text" name="home_description" value="{{$about->home_description}}" class="form-control" id="home" placeholder="Home Description">
                        {{$errors->first('home_description','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="blog_description">Blog Description</label>
                        <input type="text" name="blog_description" value="{{$about->blog_description}}" class="form-control" id="blog_description" placeholder="Blog Description">
                        {{$errors->first('blog_description','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div> --}}
            {{-- <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="how_to_buy_description">How To Buy Description</label>
                        <input type="text" name="how_to_buy_description" value="{{$about->how_to_buy_description}}" class="form-control" id="how_to_buy_description" placeholder="How To Buy Description">
                        {{$errors->first('how_to_buy_description','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="order_description">Order Description</label>
                        <input type="text" name="order_description" value="{{$about->order_description}}" class="form-control" id="order_description" placeholder="Order Description">
                        {{$errors->first('order_description','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div> --}}
            {{-- <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="contact_description">Contact Description</label>
                        <input type="text" name="contact_description" value="{{$about->contact_description}}" class="form-control" id="contact_description" placeholder="Contact Description">
                        {{$errors->first('contact_description','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="product_description">Product Description</label>
                        <input type="text" name="product_description" value="{{$about->product_description}}" class="form-control" id="product_description" placeholder="Product Description">
                        {{$errors->first('product_description','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div> --}}
            {{--  
            <div class="form-group">
                <label for="picture">Picture</label><br/>
                <input type="text" id="txtSelectedFile" class="form-control" name="picture" readonly="readonly" onfocus="this.blur()" style="border:1px solid #ccc;cursor:pointer;padding:4px;" value="{{$about->picture}}">
                {{$errors->first('picture','<p class="text-red">:message</p>')}}<br/>
                <a href="javascript:openCustomRoxy()">
                    <?php $paths = public_path($about->picture); ?>
                    @if(!empty($about->picture) && is_file($paths))
                        <img src="{{asset($about->picture)}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;" alt="img"/>
                    @else
                        <img src="{{asset('assets/store/no_image.png')}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;" alt="img"/>
                    @endif
                </a>
                <div id="roxyCustomPanel" style="display: none;">
                  <iframe src="/fileman/index.html?integration=custom&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0"></iframe>
                </div>
                <p class="help-block">Click to select a picture.</p>
            </div>   --}}
        </div><!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary waiting">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop