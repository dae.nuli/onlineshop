@extends('backend.layouts.content')

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/users/insert', 'method'=>'POST'))}}
        <div class="box-body">  
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="nama">Name</label>
                        <input type="text" name="name" value="{{Input::old('name')}}" class="form-control" id="name" placeholder="Name">
                        {{$errors->first('name','<p class="text-red">:message</p>')}}
                    </div>

                    <div class="col-xs-6">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{Input::old('email')}}" class="form-control" id="email" placeholder="Email Address">
                        {{$errors->first('email','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="phone_number">Phone Number</label>
                        <input type="text" name="phone_number" value="{{Input::old('phone_number')}}" class="form-control" id="phone_number" placeholder="Phone Number">
                        {{$errors->first('phone_number','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">                
                        <label for="bbm">BBM</label>
                        <input type="text" name="bbm" value="{{Input::old('bbm')}}" class="form-control" id="bbm" placeholder="BBM Pin">
                        {{$errors->first('bbm','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="facebook">Facebook</label>
                        <input type="text" name="facebook" value="{{Input::old('facebook')}}" class="form-control" id="facebook" placeholder="https://facebook.com/arinigrafika">
                        {{$errors->first('facebook','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="twitter">Twitter</label>
                        <input type="text" name="twitter" value="{{Input::old('twitter')}}" class="form-control" id="twitter" placeholder="https://twitter.com/arinigrafika">
                        {{$errors->first('twitter','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="instagram">Instagram</label>
                        <input type="text" name="instagram" value="{{Input::old('instagram')}}" class="form-control" id="instagram" placeholder="https://instagram.com/arinigrafika">
                        {{$errors->first('instagram','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="group">Group</label>
                        <select class="form-control" id="group" name="group">
                            <option value="">-Select Group-</option>
                            @foreach($group as $row)
                                @if($row->id == Input::old('group'))
                                    <option value="{{Input::old('group')}}" selected>{{$row->group_name}}</option>
                                @else
                                    <option value="{{$row->id}}">{{$row->group_name}}</option>
                                @endif
                            @endforeach
                        </select>
                        {{$errors->first('group','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        {{$errors->first('password','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="col-xs-6">
                        <label for="retype_password">Retype Password</label>
                        <input type="password" name="retype_password" class="form-control" id="retype_password" placeholder="Retype Password">
                        {{$errors->first('retype_password','<p class="text-red">:message</p>')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" id="status" name="status">
                    @if(1 == Input::old('status'))
                        <option value="1" selected>Active</option>
                        <option value="0">Not Active</option>
                    @else
                        <option value="1">Active</option>
                        <option value="0" selected>Not Active</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <textarea id="address" name="address" class="form-control" style="height:200px" rows="3" placeholder="Address ...">{{Input::old('address')}}</textarea>
                {{$errors->first('address','<p class="text-red">:message</p>')}}
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/users')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop