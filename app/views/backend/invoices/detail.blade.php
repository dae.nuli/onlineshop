@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script src="{{asset('assets/js/accounting.min.js')}}"></script>
    <link href="{{asset('assets/css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('assets/css/select2.js')}}"></script>

    <link href="{{asset('assets/css/bootstrap-editable.css')}}" rel="stylesheet">
    <script src="{{asset('assets/js/bootstrap-editable.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.datetimepicker.css')}}"/ >
    <script src="{{asset('assets/js/jquery.datetimepicker.js')}}"></script>
    <script type="text/javascript">
    jQuery('.datetimepicker2').datetimepicker({
        closeOnDateSelect:true,
        format:'Y-m-d',
        timepicker:false
    });
    $(document).ready(function() {
        $.fn.editable.defaults.mode = 'inline';
        $.fn.editableform.buttons = '<button type="submit" class="btn btn-primary btn-sm editable-submit"><i class="fa fa-save"></i></button><button type="button" class="btn btn-default btn-sm editable-cancel"><i class="fa fa-times"></i></button>';

            $('.item-edit').editable({
                url : '/admin/orders/item/'+{{$orders->id}},
                ajaxOption:{
                    type : 'POST'
                },
                success: function(data, config){
                    $('.total').html(accounting.formatNumber(data.data.total, 0, ".", ""));
                    $(".subtotal[id='"+data.data.id+"']").html(accounting.formatNumber(data.data.subtotal, 0, ".", ""));
                },
                inputclass : 'form-control'
            });

            $('.item-edit-product').editable({
                url : '/admin/orders/item/'+{{$orders->id}},
                ajaxOption:{
                    type : 'POST'
                },
                success: function(data, config){
                    $('.total').html(accounting.formatNumber(data.data.total, 0, ".", ""));
                    $(".subtotal[id='"+data.data.id+"']").html(accounting.formatNumber(data.data.subtotal, 0, ".", ""));
                    $(".price[id='"+data.data.id+"']").html(accounting.formatNumber(data.data.item_price, 0, ".", ""));
                },
                source: [
                @foreach($products as $row)
                      {value: '{{$row->code}}', text: '{{$row->code}}'},
                @endforeach
                ],
                select2: {
                    width: 200,
                    placeholder: 'Select Product'
                }
            });
    });
    </script>

@stop
 

@section('body-content-child')
{{Form::open(array('url'=>'admin/orders/detail/'.$orders->id, 'method'=>'POST', 'files'=>true))}}
<div class="col-xs-12">
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Client</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <p>{{$client->name}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email</label>
                        <p>{{$client->email}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Phone</label>
                        <p>{{$client->phone}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Address</label>
                        <textarea class="form-control" rows="3" disabled="">{{$client->address}}</textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    {{-- <h3 class="box-title">Client</h3> --}}
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Order ID</label>
                        <p>{{$orders->code_order}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Order Date</label>
                        <p>{{date('d F Y, H:i:s',strtotime($orders->created_at))}}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Total</label>
                        <p>Rp <span class="total">{{number_format($orders->total,0,",",".")}}</span></p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>

        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-header">
                    <h5 class="box-title" style="padding-bottom:0">Creating</h5>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Start Date</label>
                        <input type="text" class="form-control datetimepicker2" value="{{$orders->start_create}}" name="start_date" placeholder="{{date('Y-m-d')}}">
                        {{$errors->first('start_date','<p class="text-red">:message</p>')}}
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Finish Date</label>
                        <input type="text" class="form-control datetimepicker2" value="{{(strtotime($orders->finish_create))?$orders->finish_create:''}}" name="finish_date" placeholder="{{date('Y-m-d')}}">
                        {{$errors->first('finish_date','<p class="text-red">:message</p>')}}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Note</label>
                        <textarea class="form-control" rows="3" name="note" placeholder="Note of operator"></textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</div>
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title pull-left">Items</h3>
            <div class="pull-right" style="margin-left:5px">
                <a style="margin:10px 10px 0 10px;color:white" data-idorder="{{$orders->id}}" class="btn btn-sm btn-primary add-item">Add Item</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Note</th>
                        <th>Subtotal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detail as $row)
                    <?php //$price = Helper::ProductPrice($row->code_product); ?>
                    <?php $price = $row->item_price; ?>
                    <?php $subtotal = ($row->qty*$price); ?>
                    <tr>
                        <td><span class="item-edit" data-order="{{$orders->id}}" data-pk="{{$row->id}}" data-name="qty">{{$row->qty}}</span></td>
                        <td><span class="item-edit-product" data-order="{{$orders->id}}" data-type="select2" data-value="{{$row->code_product}}" data-pk="{{$row->id}}" data-name="code_product">{{$row->code_product}}</span></td>
                        <td>Rp <span class="price" id="{{$row->id}}">{{number_format($price,0,",",".")}}</span></td>
                        <td>{{$row->note}}</td>
                        <td>Rp <span class="subtotal" id="{{$row->id}}">{{number_format($subtotal,0,",",".")}}</span></td>
                        <td><div class="btn-group"><a href="{{URL::to('admin/orders/data/'.$row->id)}}" class="btn btn-sm btn-default"><i class="fa fa-info-circle"></i></a><a href="{{URL::to('admin/orders/delete-item/'.$row->id)}}" class="btn btn-sm btn-default delete"><i class="fa fa-trash-o"></i></a></div></td>
                    </tr> 
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
 
<div class="col-xs-12">
    <!-- general form elements -->
    <div class="box no-border" style="padding:10px">
        <a href="{{URL::to('admin/orders')}}" class="btn btn-default">Cancel</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{{Form::close()}}
<div id="open-modal"></div>
@stop