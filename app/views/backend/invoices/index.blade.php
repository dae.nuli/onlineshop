@extends('backend.layouts.content')

@section('header-content')
<div class="pull-right" style="margin-left:5px">
    <a href="{{URL::to('admin/invoices/create')}}" class="btn btn-primary">Create</a>
</div>
@stop

@section('body-content')
@if(Session::has('invoices'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('invoices')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover images">
            <thead>
            <tr>
                <th>Order ID</th>
                <th>Client Name</th>
                <th>Order Date</th>
                <th>Total</th>
                <th>Payment Status</th>
                <th>Order Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($invoices as $row)
            <tr>
                <td><a href="{{URL::to('admin/invoices/detail/'.$row->id)}}">{{$row->code_order}}</a></td>
                <td><a href="{{URL::to('admin/clients/detail/'.$row->id_client)}}">{{$row->client->name}}</a></td>
                <td>{{date('d F Y, H:i:s',strtotime($row->created_at))}}</td>
                <td>Rp {{number_format($row->total,0,",",".")}}</td>
                <td>{{($row->payment_status)?'<span class="label label-success">Paid</span>':'<span class="label label-warning">Not Paid</span>'}}</td>
                <td>
                    @if($row->order_status==2)
                        <span class="label label-success">Finish</span>
                    @else
                        <span class="label label-info">On Process</span>
                    @endif
                </td>
                <td>
                    <a href="{{URL::to('admin/invoices/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    <a href="{{URL::to('admin/invoices/print/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-print"></i> Print</a>
                    <a href="{{URL::to('admin/invoices/delete-order/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$invoices->links()}}
    </div>
</div>
@stop