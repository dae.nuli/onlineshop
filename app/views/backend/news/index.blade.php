@extends('backend.layouts.content')

@section('header-content')
<div class="pull-right" style="margin-left:5px;">
    <a href="{{URL::to('admin/news/create')}}" class="btn btn-primary">Create</a>
</div>

{{Form::open(array('url'=>'admin/news', 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Search news title">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('news'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('news')}}.
    </div>
@endif

@if(Session::has('news_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('news_alert')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Title</th>
                <th>Views</th>
                <th>Category</th>
                <th>Status</th>
                <th>Created by</th>
                <th>Modified at</th>
                <th>Action</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($news as $row)
            <?php $total_article = Helper::count_views($row->id,2); ?>
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->title}}</td>
                <td>{{$total_article}}</td>
                <td>{{$row->category->name}}</td>
                <td>{{($row->publish) ? '<div class="label bg-green">Published</div>' : '<div class="label bg-yellow">Draft</div>'}}</td>
                <td>{{$row->author->name}}</td>
                <td>{{date('d F Y, H:i:s',strtotime($row->updated_at))}}</td>
                <td>
                    @if($row->publish)
                        <a href="{{URL::to('admin/news/draft/'.$row->id)}}" class="btn btn-warning btn-xs drafted"><i class="fa fa-fw fa-times-circle"></i> Draft</a>
                    @else
                        <a href="{{URL::to('admin/news/publish/'.$row->id)}}" class="btn btn-success btn-xs published"><i class="fa fa-fw  fa-check-circle"></i> Publish</a>
                    @endif

                    <a href="{{URL::to('admin/news/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    <a href="{{URL::to('admin/news/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                    <!-- <a href="">Hapus</a> -->
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$news->links()}}
    </div>
</div>
@stop