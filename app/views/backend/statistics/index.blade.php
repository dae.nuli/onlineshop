@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src="{{asset('assets/js/Chart.js')}}"></script>
    <script type="text/javascript">
    $(function () {
        var data = {
            labels: [{{$month}}],
            datasets: [
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [{{$visitor}}]
                }
            ]
        };
        // Get context with jQuery - using jQuery's .get() method.
        var ctx = $("#myChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var myNewChart = new Chart(ctx).Line(data);
    })
    </script>
@stop

@section('body-content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Unique visitors</h3>
            <h3 class="box-title pull-right">
                {{Form::open(array('url' => 'admin/statistics', 'method' => 'post'))}}
                <select class="form-control" name="unique" onchange="this.form.submit()">
                    @foreach($year_range as $row)
                        <option value="{{$row}}" {{($year_unique==$row) ? 'selected' : ''}}>{{$row}}</option>
                    @endforeach
                </select>
                {{Form::close()}}
            </h3>
        </div>        
        <div class="box-body chart-responsive">
            <canvas id="myChart" style="width:100%; height:300px;" height="400"></canvas>
        </div><!-- /.box-body -->
    </div>

@stop