@extends('backend.layouts.content')

@section('end-script')
    @parent 
    <script type="text/javascript" src="{{asset('assets/js/AdminLTE/jquery.blockUI.js')}}"></script>
    <script type="text/javascript">
      $(function() {
        $( "#sortable" ).sortable({
            opacity:0.6,
            cursor:'move',
            update:function(){
                waiting();
                var order = $(this).sortable('serialize');
                $.post("/admin/product_category/drag",order,function(response){
                    $.unblockUI(); 
                });
            }
        });
        $( "#sortable" ).disableSelection();
      });
    function waiting(){
        $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        },baseZ:9999 });
    }
    $('.tunjuk').mouseover(function(){
        $(this).css('cursor','move');
    })
    </script>
@stop

@section('header-content')
<div class="pull-right">
    <a href="{{URL::to('admin/product_category/create')}}" class="btn btn-primary">Create</a>
</div>
@stop

@section('body-content')
@if(Session::has('product_category'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('product_category')}}.
    </div>
@endif

@if(Session::has('product_category_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('product_category_alert')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover sorted_table">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Name</th>
                <th>Total Product</th>
                <th>Modified at</th>
                <th>Action</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody id="sortable">
            @foreach($category as $row)
            <?php $total = Helper::TotalOfProduct($row->id); ?>
            <tr id="id-{{$row->id}}" class="tunjuk">
                <td></td>
                <td>{{$row->name}}</td>
                <td>{{$total}}</td>
                <td>{{date('d F Y, H:i:s',strtotime($row->updated_at))}}</td>
                <td>
                    <a href="{{URL::to('admin/product_category/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    <a href="{{URL::to('admin/product_category/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                    <!-- <a href="">Hapus</a> -->
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$category->links()}}
    </div>
</div>
@stop