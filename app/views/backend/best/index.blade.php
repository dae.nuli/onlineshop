@extends('backend.layouts.content')

@section('end-script')
    @parent
    <script src='{{asset("assets/js/jquery.museum.js")}}'></script>
    <script>
        $(document).ready(function() {
            var options = {
                namespace: "msm",       // for custom CSS class namespacing
                padding: 25,            // in case you would like a specific amount of padding on the lightbox
                disable_url_hash: true, // disable using hashes in case your website already uses URL hashes for states
            };
            $.museum($('.images img'),options);
        });
    </script>
@stop

@section('header-content')
{{Form::open(array('url'=>'admin/best/filter', 'method'=>'GET'))}}
<div class="pull-right">
    <button type="submit" class="btn btn-warning">Clear</button>
</div>
{{Form::close()}}

{{Form::open(array('url'=>'admin/best/filter', 'method'=>'GET'))}}
<div class="col-lg-5 pull-right" style="width:350px">
    <div class="pull-right" style="margin-left:5px">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    <div class="input-group">
        <input type="text" name="BS_date_filter" class="form-control pull-right date-filter" value="{{Session::get('BS_date_filter')}}" style="width: 200px;"/>
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
    </div>
</div>
{{Form::close()}}

{{Form::open(array('url'=>'admin/best', 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="col-lg-5 pull-right">
    <div class="input-group">
        <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Search product code">
        <div class="input-group-btn">
            <button class="btn btn-default"><i class="fa fa-search"></i></button>
        </div>
    </div>
</div>
{{Form::close()}}

@stop

@section('body-content')
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover images">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Code</th>
                <th>Picture</th>
                <th>Price</th>
                <th>Viewed</th>
                <th>Created by</th>
                <th>Order at</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($best as $row)
            <tr>
                <td>{{$nomor++}}.</td>
                <td>{{$row->code_product}}</td>
                <td>
                    <?php
                    $pic   = Seller::GetPicture($row->code_product);
                    $paths = public_path($pic);
                    ?>
                    @if(!empty($pic) && is_file($paths))
                        <img width="100" src="{{asset($pic)}}" alt="img"/>
                    @else
                        <img width="100" src="{{asset('assets/store/no_image.png')}}" alt="img"/>
                    @endif 
                </td>
                <td>Rp {{number_format($row->item_price,0,",",".")}}</td>
                <td>{{Seller::GetViewer($row->code_product)}}</td>
                <td>{{Seller::GetUserName($row->code_product)}}</td>
                <td>{{date('d F Y, H:i:s',strtotime($row->created_at))}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$best->links()}}
    </div>
</div>
@stop

@section('end-script')
    @parent

    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker/daterangepicker-bs3.css')}}"/ >
    <script src="{{asset('assets/js/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript">
    $('.date-filter').daterangepicker({
        format: 'YYYY-MM-DD',
        separator:' / '
    });
    // $(".date-filter").keypress(function(event) {event.preventDefault();});
    </script>
@stop