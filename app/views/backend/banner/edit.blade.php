@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script type="text/javascript" src="{{ asset('fileman/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('fileman/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function openCustomRoxy(){
          $('#roxyCustomPanel').dialog({modal:true, width:875,height:600});
        }
        function closeCustomRoxy(){
          $('#roxyCustomPanel').dialog('close');
        }        
    </script>

    <script type="text/javascript" src="{{asset('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        tinymce.init({
            selector: "textarea"
         });
    });
    </script>
    <script type="text/javascript">
    $(document).on('mouseover','.headerimage',function(){
        var y1     = $('.product-picture-wrapper').height();   
        var y2     = $('.headerimage').height();
        // var p      = $(".headerimage").attr("style");
        // var Y      = p.split("top:");
        // var Z      = Y[1].split(";");
        // var posisi = Z[0];

        $(this).draggable({
            scroll:false,
            axis:"y",
            drag: function(event,ui){
                if(ui.position.top >= 0){
                    ui.position.top = 0;
                }else if(ui.position.top <= y1 - y2){
                    ui.position.top = y1 - y2;
                }
                $('.picture-position').val(ui.position.top);

                console.log(ui.position.top);
            },stop:function(event,ui){

            }
        });
    })
    </script>

@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/banner/update/'.$banner->id, 'method'=>'POST', 'files'=>true))}}
        <div class="box-body">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" value="{{$banner->title}}" class="form-control" id="title" placeholder="Banner Title">
                {{$errors->first('title','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="body">Content</label>
                <textarea id="body" name="content" class="form-control textarea" style="height:200px" rows="3" placeholder="Content ...">{{$banner->content}}</textarea>
                {{$errors->first('content','<p class="text-red">:message</p>')}}
            </div> 
            <div class="form-group">
                <label for="picture">Picture</label><br/>
                <input type="text" id="txtSelectedFile" class="form-control" name="picture" readonly="readonly" onfocus="this.blur()" style="border:1px solid #ccc;cursor:pointer;padding:4px;" value="{{$banner->picture}}">
                {{$errors->first('picture','<p class="text-red">:message</p>')}}<br/>
                <div class="product-picture-wrapper">
                    <a href="javascript:openCustomRoxy()">
                        <?php $paths = public_path($banner->picture); ?>
                        @if(!empty($banner->picture) && is_file($paths))
                            <img src="{{asset($banner->picture)}}" id="customRoxyImage" class="img-responsive headerimage" alt="img" style="position: relative; top:{{$banner->position}}px"/>
                        @else
                            <img src="{{asset('assets/store/no_image.png')}}" id="customRoxyImage" alt="img"/>
                        @endif
                    </a>
                </div>
                <input type="hidden" name="position" class="picture-position" value="{{$banner->position}}">
                <div id="roxyCustomPanel" style="display: none;">
                  <iframe src="/fileman/index.html?integration=custom&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0"></iframe>
                </div>
                <p class="help-block">Click to select a picture.</p>
            </div>  
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/banner')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary waiting">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop