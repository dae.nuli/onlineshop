<!-- sidebar menu: : style can be found in sidebar.less -->
<?php
    $uri3  = Request::segment(3);
    $uri   = Request::segment(2);
    $uri1  = Request::segment(1);
    $admin = Session::get('admin');
?>
                    <ul class="sidebar-menu">
                        <li @if($uri1=='admin' && $uri=='home') class="active" @endif>
                            <a href="{{URL::to('admin')}}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        @if($admin['level']==1)
                        <li class="treeview @if($uri=='news_category'||$uri=='news') active @endif">
                            <a href="#">
                                <i class="fa fa-file-o"></i>
                                <span>Blog</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='news_category') class="active" @endif><a href="{{URL::to('admin/news_category')}}"><i class="fa fa-angle-double-right"></i> Category</a></li>
                                <li @if($uri=='news') class="active" @endif><a href="{{URL::to('admin/news')}}"><i class="fa fa-angle-double-right"></i> List</a></li>
                            </ul>
                        </li>
                        <li class="treeview @if($uri=='products'||$uri=='product_category'||$uri=='product_subcategory') active @endif">
                            <a href="#">
                                <i class="fa fa-list-alt"></i>
                                <span>Products</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='product_category') class="active" @endif><a href="{{URL::to('admin/product_category')}}"><i class="fa fa-angle-double-right"></i> Category</a></li>
                                <li @if($uri=='products') class="active" @endif><a href="{{URL::to('admin/products')}}"><i class="fa fa-angle-double-right"></i> List</a></li>
                            </ul>
                        </li>


                        <li @if($uri=='banner') class="active" @endif>
                            <a href="{{URL::to('admin/banner')}}">
                                <i class="fa fa-desktop"></i> <span>Banner</span>
                            </a>
                        </li>
                        
                        <li @if($uri=='statistics') class="active" @endif>
                            <a href="{{URL::to('admin/statistics')}}">
                                <i class="fa fa-signal"></i> <span>Statistics</span>
                            </a>
                        </li>
                        <li @if($uri=='how') class="active" @endif>
                            <a href="{{URL::to('admin/how')}}">
                                <i class="fa fa-question-circle"></i> <span>How to Buy</span>
                            </a>
                        </li>
                        <li @if($uri=='users') class="active" @endif>
                            <a href="{{URL::to('admin/users')}}">
                                <i class="fa fa-users"></i> <span>Users</span>
                            </a>
                        </li>
                        <li @if($uri=='about') class="active" @endif>
                            <a href="{{URL::to('admin/about')}}">
                                <i class="fa fa-info-circle"></i> <span>About</span>
                            </a>
                        </li>
                        
                        <li class="treeview @if($uri=='group' || $uri=='permission') active @endif">
                            <a href="#">
                                <i class="fa fa-list-ul"></i>
                                <span>Group Permission</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if($uri=='group') class="active" @endif>
                                    <a href="{{URL::to('admin/group')}}">
                                        <i class="fa fa-angle-double-right"></i> Group
                                    </a>
                                </li>
                                <li @if($uri=='permission') class="active" @endif>
                                    <a href="{{URL::to('admin/permission')}}">
                                        <i class="fa fa-angle-double-right"></i> Permission
                                    </a>
                                </li>
                            </ul> 
                        </li>
                        @else
                        {{Permission::ShowMenu($admin['level'],$uri,$uri1)}}
                        @endif
                    </ul>