@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script type="text/javascript" src="{{ asset('fileman/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('fileman/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        // function openCustomRoxy2(){
        //   $('#roxyCustomPanel2').dialog({modal:true, width:875,height:600});
        // }
        // function closeCustomRoxy2(){
        //   $('#roxyCustomPanel2').dialog('close');
        // }
        function openCustomRoxy(){
          $('#roxyCustomPanel').dialog({modal:true, width:875,height:600});
        }
        function closeCustomRoxy(){
          $('#roxyCustomPanel').dialog('close');
        }        
    </script>

    <script type="text/javascript" src="{{asset('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        tinymce.init({
            selector: "textarea"
         });
    });
    </script>

@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/product_subcategory/create-product', 'method'=>'POST', 'files'=>true))}}
        <input type="hidden" name="subcategory" value="{{Input::get('sub')}}"/>
        <div class="box-body">
            <div class="form-group">
                <label for="category">Sub Category</label>
                <input type="text" class="form-control" value="{{$subcategory->name}}" disabled="">
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                <input type="text" name="price" value="{{Input::old('price')}}" class="form-control" id="price" placeholder="Product Price">
                {{$errors->first('price','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="body">Content</label>
                <textarea id="body" name="content" class="form-control textarea" style="height:200px" rows="3" placeholder="Content ...">{{Input::old('content')}}</textarea>
                {{$errors->first('content','<p class="text-red">:message</p>')}}
            </div> 
            <div class="form-group">
                <label for="picture">Picture</label><br/>
                <input type="text" id="txtSelectedFile" class="form-control" name="picture" readonly="readonly" onfocus="this.blur()" style="border:1px solid #ccc;cursor:pointer;padding:4px;">
                {{$errors->first('picture','<p class="text-red">:message</p>')}}<br/>
                <a href="javascript:openCustomRoxy()"><img src="{{asset('assets/store/no_image.png')}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;"></a>
                <div id="roxyCustomPanel" style="display: none;">
                  <iframe src="/fileman/index.html?integration=custom&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0"></iframe>
                </div>
                <p class="help-block">Click to select a picture.</p>
            </div>  
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/product_subcategory/detail/'.$subcategory->id)}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop