@extends('backend.layouts.content')

@section('end-script')
    @parent

    <script type="text/javascript" src="{{ asset('fileman/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('fileman/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        // function openCustomRoxy2(){
        //   $('#roxyCustomPanel2').dialog({modal:true, width:875,height:600});
        // }
        // function closeCustomRoxy2(){
        //   $('#roxyCustomPanel2').dialog('close');
        // }
        function openCustomRoxy(){
          $('#roxyCustomPanel').dialog({modal:true, width:875,height:600});
        }
        function closeCustomRoxy(){
          $('#roxyCustomPanel').dialog('close');
        }        
    </script>

@stop

@section('body-content')
<div class="box">
    {{Form::open(array('url'=>'admin/product_subcategory/edit/'.$subcategory->id, 'method'=>'POST'))}}
        <div class="box-body">
            <div class="form-group">
                <label for="category">Category</label>
                <select class="form-control" id="category" name="category">
                    <option value="">-Select Category-</option>
                    @foreach($category as $row)
                        @if($subcategory->id_category == $row->id)
                            <option value="{{$subcategory->id_category}}" selected>{{$row->name}}</option>
                        @else
                            <option value="{{$row->id}}">{{$row->name}}</option>
                        @endif
                    @endforeach
                </select>
                {{$errors->first('category','<p class="text-red">:message</p>')}}
            </div> 
            <div class="form-group">
                <label for="nama">Name</label>
                <input type="text" name="name" value="{{$subcategory->name}}" class="form-control" id="name" placeholder="Name">
                {{$errors->first('name','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="prefix">Code Prefix</label>
                <input type="text" name="code_prefix" value="{{$subcategory->code_prefix}}" class="form-control" id="prefix" placeholder="Code Prefix">
                {{$errors->first('code_prefix','<p class="text-red">:message</p>')}}
            </div>
            <div class="form-group">
                <label for="picture">Picture</label><br/>
                <input type="text" id="txtSelectedFile" class="form-control" name="picture" readonly="readonly" onfocus="this.blur()" style="border:1px solid #ccc;cursor:pointer;padding:4px;" value="{{$subcategory->picture}}">
                {{$errors->first('picture','<p class="text-red">:message</p>')}}<br/>
                <a href="javascript:openCustomRoxy()">
                    <?php $paths = public_path($subcategory->picture); ?>
                    @if(!empty($subcategory->picture) && is_file($paths))
                        <img src="{{asset($subcategory->picture)}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;" alt="img"/>
                    @else
                        <img src="{{asset('assets/store/no_image.png')}}" id="customRoxyImage" style="max-width:450px; border:1px solid grey;" alt="img"/>
                    @endif
                </a>
                <div id="roxyCustomPanel" style="display: none;">
                  <iframe src="/fileman/index.html?integration=custom&txtFieldId=txtSelectedFile" style="width:100%;height:100%" frameborder="0"></iframe>
                </div>
                <p class="help-block">Click to select a picture.</p>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <a href="{{URL::to('admin/product_subcategory')}}" class="btn btn-default">{{trans('button.bc')}}</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {{Form::close()}}
</div>
@stop