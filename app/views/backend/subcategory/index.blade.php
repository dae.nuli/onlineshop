@extends('backend.layouts.content')

@section('header-content')
<div class="pull-right">
    <a href="{{URL::to('admin/product_subcategory/create')}}" class="btn btn-primary">Create</a>
</div>
<div class="pull-right" style="margin-right:5px;margin-left:5px">
    <a href="#" data-toggle="modal" data-target="#compose-modal" data-method="filter" class="btn btn-primary filter">Filter</a>
</div>

{{Form::open(array('url'=>'admin/product_subcategory', 'method'=>'GET'))}}
<?php $search = Input::get('search'); ?>
<div class="input-group">
    <input type="text" name="search" value="{{($search?$search:'')}}" class="form-control pull-right" style="width: 150px;" placeholder="Search sub category">
    <div class="input-group-btn">
        <button class="btn btn-default"><i class="fa fa-search"></i></button>
    </div>
</div>
{{Form::close()}}
@stop

@section('body-content')
@if(Session::has('product_subcategory'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> {{Session::get('product_subcategory')}}.
    </div>
@endif

@if(Session::has('product_subcategory_alert'))
    <div class="alert alert-warning alert-dismissable">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Warning!</b> {{Session::get('product_subcategory_alert')}}.
    </div>
@endif
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 50px">#</th>
                <th>Name</th>
                <th>Code Prefix</th>
                <th>Category</th>
                <th>Total Product</th>
                <th>Created by</th>
                <th>Modified at</th>
                <th>Action</th>
            </tr>
            </thead>
            <?php 
            if(Input::get('page')){
                $page = Input::get('page');
            }else{
                $page = 1;
            }
            $nomor = $page + ($page-1) * ($limit-1);
            ?>
            <tbody>
            @foreach($subcategory as $row)
            <?php $total = Helper::ProductTotal($row->id); ?>
            <tr>
                <td>{{$nomor++}}.</td>
                <td><a href="{{URL::to('admin/product_subcategory/detail/'.$row->id)}}">{{$row->name}}</a></td>
                <td>{{$row->code_prefix}}</td>
                <td>{{$row->categories->name}}</td>
                <td>{{$total}}</td>
                <td>{{$row->author->name}}</td>
                <td>{{date('d F Y, H:m:i',strtotime($row->updated_at))}}</td>
                <td>
                    <a href="{{URL::to('admin/product_subcategory/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    <a href="{{URL::to('admin/product_subcategory/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                    <!-- <a href="">Hapus</a> -->
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {{$subcategory->links()}}
    </div>
</div>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Filter Data</h4>
            </div>
            <form action="{{URL::to('admin/product_subcategory/filter')}}" method="POST">
                <div class="modal-body">
                    <div class="box-body">  
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select class="form-control" id="category" name="category">
                                <?php #$session = Session::get('category'); ?>
                                <option value="all" @if($session=='all') selected @endif>All</option>
                                @foreach($category as $row)
                                    <option value="{{$row->id}}" @if($session==$row->id) selected @endif>{{$row->name}}</option>
                                @endforeach
                            </select>
                            {{$errors->first('category','<p class="text-red">:message</p>')}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>
@stop