<?php

class RentalKamera extends BaseController {
	public $limit = 12;
	public $ip;

	public function __construct()
	{
		 $this->ip = $_SERVER['REMOTE_ADDR'];
	}

	public function index()
	{
		View::share('title','Home');
		// FrontEnd::AddVisitor($this->ip,'',4);
		$data['displayProduct'] = ProductsModel::orderBy('id','desc')->take(8)->get();
		$data['banner']         = BannerModel::where('status',1)->orderBy('id','desc')->take(3)->get();
		// print_r($data['banner']);
		// exit();
		return View::make('eshopper.home.index',$data);
	}

	public function blog($slug=null)
	{
		View::share('title','Blog');
		if(!empty($slug)){
			$data['detail'] = NewsModel::where('slug',$slug)->where('publish',1)->first();
			if(!empty($data['detail'])){
				// FrontEnd::AddVisitor($this->ip,$data['detail']->id,2);
			$data['description'] = AboutModel::find(1)->blog_description;
				return View::make('front.blog.detail',$data);
			}
		}else{
			// FrontEnd::AddVisitor($this->ip,'',4);
			$data['blog'] = NewsModel::orderBy('id','desc')->where('publish',1)->paginate($this->limit);
			$data['description'] = AboutModel::find(1)->blog_description;
			return View::make('eshopper.blog.index',$data);
		}
	}

	public function category($category=null,$slug=null)
	{
		View::share('title','Product');
		if(!empty($category))
		{
			$CM = CategoryModel::where('slug',$category)->first();
			if(!empty($CM))
			{
					if(!empty($slug)){
						$data['detail']        = ProductsModel::where('id_category',$CM->id)->where('slug',$slug)->first();
						FrontEnd::AddVisitor($this->ip,$data['detail']->id);
						return View::make('eshopper.product.detail',$data);
					}else{
						// FrontEnd::AddVisitor($this->ip,'',4);
						$data['product']     = ProductsModel::where('id_category',$CM->id)->orderBy('id','desc')->paginate($this->limit);
						$data['category']    = $CM;
						return View::make('eshopper.product.index',$data);
					}
				// }
			}
		}
		// else{
		// 	$data['product']     = ProductsModel::orderBy('id','desc')->paginate($this->limit);
		// 	return View::make('eshopper.product.product',$data);
		// }
	}

	public function how()
	{
		View::share('title','How to Buy');
		$data['how'] = HowToBuyModel::find(1);
		return View::make('eshopper.how.index',$data);
	}
	public function promo($slug)
	{
		$data['promo'] = BannerModel::where('slug',$slug)->first();
		View::share('title',$data['promo']->title);
		return View::make('eshopper.promo.index',$data);
	}
	public function contact()
	{
		View::share('title','Contact Us');
		$data['description'] = AboutModel::find(1);
		return View::make('eshopper.contact.index',$data);
	}

	public function search()
	{
		View::share('title','Hasil Pencarian');
		$cari = Input::get('cari');
		if(!empty($cari)){
			$data['search'] = SubCategoryModel::where('name','LIKE',"$cari%")->paginate($this->limit);
			if(count($data['search'])>0){
				return View::make('front.search.index',$data);
			}else{
				return View::make('front.search.not_found');
			}
		}else{
			return View::make('front.search.not_found');
		}
	}

	public function postContact()
	{
		$rules = array(
			'nama_lengkap'    => 'required',
			'email'           => 'required|email',
			'nomor_handphone' => 'required',
			'pesan'           => 'required',
			'captcha'         => 'required|captcha',
		);
		$pesan = array(
			'nama_lengkap.required'    => 'Nama lengkap tidak boleh kosong',
			'email.required'           => 'Email tidak boleh kosong',
			'email.email'              => 'Email anda tidak sesuai dengan format penulisan email',
			'nomor_handphone.required' => 'Nomor handphone tidak boleh kosong',
			'pesan.required'           => 'Alamat tidak boleh kosong',
			'captcha.required'         => 'Captcha tidak boleh kosong',
			'captcha.captcha'          => 'Captcha yang anda masukan salah'
		);
		$valid = Validator::make(Input::all(),$rules,$pesan);
		if($valid->fails())
		{
			return Redirect::to('contact')->withErrors($valid)->withInput();
		}else{
			$MM            = new MessageModel;
			$MM->name      = Input::get('nama_lengkap');
			$MM->email     = Input::get('email');
			$MM->handphone = Input::get('nomor_handphone');
			$MM->content   = Input::get('pesan');
			$MM->save();
			return Redirect::to('contact')->with('contact','Terima Kasih');
		}
	}

	public function order()
	{
		View::share('title','Data Mempelai');
		// FrontEnd::AddVisitor($this->ip,'',4);
		$id = Input::get('id');
		if(!empty($id)){
			$PM = ProductsModel::where('code',$id)->first();
			if(!empty($PM)){
				$data['product'] = $PM;
				$data['agama'] = ReligionModel::all();
				$data['description'] = AboutModel::find(1)->order_description;
				return View::make('front.order.index',$data);
			}
		}else{
			$data['agama'] = ReligionModel::all();
				$data['description'] = AboutModel::find(1)->order_description;
			return View::make('front.order.index',$data);
		}
	}

	public function postOrder()
	{
		
			// $codeProduct = Input::get('code_produk');
			// return $codeProduct[0];
		// echo "<pre>";
		// print_r(Input::all());
		// echo "</pre>";
		// return '';
		$rules = array(
			'nama_lengkap'              => 'required',
			'email'                     => 'required|email',
			'nomor_handphone'           => 'required',
			'alamat_pemesan'            => 'required',
			
			'code_produk.0'               => 'required',
			'jumlah_produk.0'             => 'required',
			'nama_mempelai_wanita'      => 'required',
			'nama_ayah_mempelai_wanita' => 'required',
			'nama_ibu_mempelai_wanita'  => 'required',
			'alamat_mempelai_wanita'    => 'required',
			
			'nama_mempelai_pria'        => 'required',
			'nama_ayah_mempelai_pria'   => 'required',
			'nama_ibu_mempelai_pria'    => 'required',
			'alamat_mempelai_pria'      => 'required',
			
			'akad_tanggal'              => 'required',
			'akad_jam'                  => 'required',
			'akad_lokasi'               => 'required',

			'resepsi_tanggal'           => 'required',
			'resepsi_jam'               => 'required',
			'resepsi_lokasi'            => 'required',

			'panggilan_wanita'          => 'required',
			'panggilan_pria'            => 'required',
			'turut_mengundang'          => 'required'
		);
		$pesan = array(
			'nama_lengkap.required'              => 'Nama lengkap tidak boleh kosong',
			'email.required'                     => 'Email tidak boleh kosong',
			'email.email'                        => 'Email anda tidak sesuai dengan format penulisan email',
			'nomor_handphone.required'           => 'Nomor handphone tidak boleh kosong',
			'alamat_pemesan.required'            => 'Alamat tidak boleh kosong',
			
			'code_produk.0.required'               => 'Kode produk tidak boleh kosong',
			'jumlah_produk.0.required'             => 'Jumlah produk tidak boleh kosong',
			'nama_mempelai_wanita.required'      => 'Nama mempelai wanita tidak boleh kosong',
			'nama_ayah_mempelai_wanita.required' => 'Nama ayah mempelai wanita tidak boleh kosong',
			'nama_ibu_mempelai_wanita.required'  => 'Nama ibu mempelai wanita tidak boleh kosong',
			'alamat_mempelai_wanita.required'    => 'Alamat mempelai wanita tidak boleh kosong',
			
			'nama_mempelai_pria.required'        => 'Nama mempelai pria tidak boleh kosong',
			'nama_ayah_mempelai_pria.required'   => 'Nama ayah mempelai pria tidak boleh kosong',
			'nama_ibu_mempelai_pria.required'    => 'Nama ibu mempelai pria tidak boleh kosong',
			'alamat_mempelai_pria.required'      => 'Alamat mempelai pria tidak boleh kosong',
			
			'akad_tanggal.required'              => 'Tanggal akad tidak boleh kosong',
			'akad_jam.required'                  => 'Jam akad tidak boleh kosong',
			'akad_lokasi.required'               => 'Lokasi tidak boleh kosong',
			
			'resepsi_tanggal.required'           => 'Tanggal resepsi tidak boleh kosong',
			'resepsi_jam.required'               => 'Jam resepsi tidak boleh kosong',
			'resepsi_lokasi.required'            => 'Lokasi resepsi tidak boleh kosong',
			
			'panggilan_wanita.required'          => 'Nama panggilan wanita tidak boleh kosong',
			'panggilan_pria.required'            => 'Nama panggilan pria tidak boleh kosong',
			'turut_mengundang.required'          => 'Turut mengundang tidak boleh kosong'
			);
		$valid = Validator::make(Input::all(),$rules,$pesan);
		if($valid->fails())
		{
			return Redirect::to('order')->withErrors($valid)->withInput();
		}else{
			$cm          = new ClientsModel;
			$cm->name    = Input::get('nama_lengkap');
			$cm->email   = Input::get('email');
			$cm->address = Input::get('nomor_handphone');
			$cm->phone   = Input::get('alamat_pemesan');
			$cm->save();

			$OrModel     = OrdersModel::orderBy('code_order','desc')->first();
			$prefix = "AG";
			if(empty($OrModel)){
				$OrderCode = $prefix."00001";
			}else{
				$number = (int)substr($OrModel->code_order, strlen($prefix), 5);
				$number++;
				$OrderCode = $prefix.sprintf("%05d", $number);
			}

			$codeProduct = Input::get('code_produk');
			$Qty         = Input::get('jumlah_produk');

			$om             = new OrdersModel;
			$om->code_order = $OrderCode;
			$om->id_client  = $cm->id;
			$om->total      = FrontEnd::OrderTotalArray($codeProduct,$Qty);
			$om->save();

			foreach ($codeProduct as $key1 => $value1) {
				$ODM               = new OrdersDetailModel;
				$ODM->id_order     = $om->id;
				$ODM->code_product = $value1;
				$ODM->qty          = $Qty[$key1];
				$ODM->item_price   = FrontEnd::ItemPrice($value1);
				$ODM->save();

				$IdODM[$key1] = $ODM->id;
			}
			// foreach ($Qty as $key2 => $value2) {
			// 	$Odm = OrdersDetailModel::find($IdODM[$key2]);
			// 	$Odm->qty = $value2;
			// 	$Odm->save();
			// }

			$OrderData                       = new OrdersDataModel;
			$OrderData->orders_detail_id     = $IdODM[0];
			$OrderData->woman_name           = Input::get('nama_mempelai_wanita');
			$OrderData->woman_father         = Input::get('nama_ayah_mempelai_wanita');
			$OrderData->woman_mother         = Input::get('nama_ibu_mempelai_wanita');
			$OrderData->woman_address        = Input::get('alamat_mempelai_wanita');
			
			$OrderData->man_name             = Input::get('nama_mempelai_pria');
			$OrderData->man_father           = Input::get('nama_ayah_mempelai_pria');
			$OrderData->man_mother           = Input::get('nama_ibu_mempelai_pria');
			$OrderData->man_address          = Input::get('alamat_mempelai_pria');
			
			$OrderData->akad_date            = Input::get('akad_tanggal');
			$OrderData->akad_time            = Input::get('akad_jam');
			$OrderData->akad_location        = Input::get('akad_lokasi');
			
			$OrderData->resepsi_date         = Input::get('resepsi_tanggal');
			$OrderData->resepsi_time         = Input::get('resepsi_jam');
			$OrderData->resepsi_location     = Input::get('resepsi_lokasi');
			
			$OrderData->nickname_woman       = Input::get('panggilan_wanita');
			$OrderData->nickname_man         = Input::get('panggilan_pria');
			$OrderData->id_religion          = Input::get('agama');
			$OrderData->important_invitation = Input::get('turut_mengundang');
			$OrderData->save();

			return Redirect::to('order')->with('order','Terima Kasih');
		}
	}

	public function not()
	{
		return View::make('front.404');
	}
}