<?php

class Profile extends BaseController {
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Profile');
		View::share('path','Edit');
		$data['profile'] = UserModel::find($this->admin['id']);
		return View::make('backend.profile.index',$data);
	}

	public function postUpdate()
	{
		$pass = Input::get('password');
		$rules['name']         = 'required';
		$rules['phone_number'] = 'required';
		$rules['address']      = 'required';
		if(!empty($pass)){
			$rules['password']        = 'required|min:5';
			$rules['retype_password'] = 'required|same:password';
		}
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/profile')->withErrors($valid);
		}else{
			$cm            = UserModel::find($this->admin['id']);
			$cm->name      = Input::get('name');
			$cm->bbm       = Input::get('bbm');
			$cm->address   = Input::get('address');
			$cm->phone     = Input::get('phone_number');
			$cm->facebook  = Input::get('facebook');
			$cm->twitter   = Input::get('twitter');
			$cm->instagram = Input::get('instagram');
			if(!empty($pass)){
				$cm->password     = md5($pass.'arinigrafikakeren');
			}
			$cm->save();
			return Redirect::to('admin/profile')->with('users','Data has been updated'); 
		}
	}
}