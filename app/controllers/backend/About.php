<?php

class About extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function getIndex()
	{
		View::share('title','About');
		View::share('path','Index');
		$data['about'] = AboutModel::find(1);
		return View::make('backend.about.index',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name'         => 'required',
			'email'        => 'required',
			'address'      => 'required',
			'facebook'     => 'required',
			'twitter'      => 'required',
			'instagram'    => 'required',
			'gplus'        => 'required',
			'bbm'          => 'required',
			'phone_number' => 'required|numeric',
			'web_keywords' => 'required',
			'latitude'     => 'required',
			'longitude'    => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/about')->withErrors($valid)->withInput();
		}else{
			$cm               = AboutModel::find(1);
			$cm->name         = Input::get('name');
			$cm->email        = Input::get('email');
			$cm->web_keywords = Input::get('web_keywords');
			$cm->address      = Input::get('address');
			$cm->facebook     = Input::get('facebook');
			$cm->twitter      = Input::get('twitter');
			$cm->instagram    = Input::get('instagram');
			$cm->bbm          = Input::get('bbm');
			$cm->gplus        = Input::get('gplus');
			$cm->phone        = Input::get('phone_number');
			$cm->lat          = Input::get('latitude');
			$cm->lng          = Input::get('longitude');
			$cm->save();
			return Redirect::to('admin/about')->with('about','Data has been updated');
		}
	}
}
