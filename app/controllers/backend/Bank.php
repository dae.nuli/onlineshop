<?php

class Bank extends BaseController {
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Bank');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$data['bank']  = BankModel::orderBy('id','desc')->paginate($this->limit);
		return View::make('backend.bank.index',$data);
	}

	public function getCreate()
	{
		View::share('path','Create');
		View::share('title','Bank');
		return View::make('backend.bank.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name' => 'required',
			'account' => 'required',
			'picture' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/bank/create')->withErrors($valid)->withInput();
		}else{
			$bm          = new BankModel;
			$bm->name    = Input::get('name');
			$bm->account = Input::get('account');
			$bm->picture = Input::get('picture');
			$bm->save();
			return Redirect::to('admin/bank')->with('bank','Data has been added');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','Bank');

		$data['bank'] = BankModel::find($id);
		return View::make('backend.bank.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$rules = array(
			'name' => 'required',
			'account' => 'required',
			'picture' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/bank/edit/'.$id)->withErrors($valid);
		}else{
			$bm          = BankModel::find($id);
			$bm->name    = Input::get('name');
			$bm->account = Input::get('account');
			$bm->picture = Input::get('picture');
			$bm->save();
			return Redirect::to('admin/bank')->with('bank','Data has been updated'); 
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$bm = BankModel::find($id);
		$bm->delete();
		return Redirect::to('admin/bank')->with('bank','Data has been deleted');
	}
}
