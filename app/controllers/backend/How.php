<?php

class How extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function getIndex()
	{
		View::share('title','How');
		View::share('path','Index');
		$data['how'] = HowToBuyModel::find(1);
		return View::make('backend.how.index',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'content'         => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/how')->withErrors($valid)->withInput();
		}else{
			$htm                 = HowToBuyModel::find(1);
			$htm->content        = Input::get('content');
			$htm->save();
			return Redirect::to('admin/how')->with('how','Data has been updated');
		}
	}
}
