<?php

class Products extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}

	public function getIndex()
	{
		View::share('title','Products');
		View::share('path','Index');
		$data['limit'] = $this->limit;
		$cari          = Input::get('search');
		if($this->admin['level'] == 1)
		{
			$qr	= ProductsModel::orderBy('id','desc');
			if($cari){
				$qr = $qr->where('name','LIKE',"%$cari%");
			}
			$qr = $qr->paginate($this->limit);		
		}else{
			$qr	= ProductsModel::where('id_user',$this->admin['id']);
			if($cari){
				$qr = $qr->where('name','LIKE',"%$cari%");
			}
			$qr = $qr->orderBy('id','desc')->paginate($this->limit);
		}
		$data['product'] = $qr;
		return View::make('backend.product.index',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{ 
		View::share('title','Products');
		View::share('path','Create');
		if($this->admin['level'] == 1){
			$category = CategoryModel::all();
		}else{
			$category = CategoryModel::where('id_user',$this->admin['id'])->first();
		} 
		$data['category'] = $category;
		return View::make('backend.product.create',$data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'category' => 'required',
			'name'     => 'required',
			'price'    => 'required|numeric',
			'content'  => 'required',
			'picture'  => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/products/create')->withErrors($valid)->withInput();
		}else{
			$id_cat = Input::get('category');

			$picture         = Input::get('picture');
			$cm              = new ProductsModel;
			$cm->id_user     = $this->admin['id'];
			$cm->id_category = $id_cat;
			$cm->name        = Input::get('name');
			$cm->price       = Input::get('price');
			$cm->note        = Input::get('content');
			$cm->picture     = ($picture?$picture:'');
			$cm->save();

			$pm = ProductsModel::find($cm->id);
			$pm->slug  = Str::slug(Input::get('name')).'-'.$cm->id;
			$pm->save();
			return Redirect::to('admin/products')->with('products','Data has been added');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('title','Products');
		View::share('path','Create');

		if($this->admin['level'] == 1)
		{
			$cm  = ProductsModel::find($id);
			$ct  = CategoryModel::all();
		}else{
			$cm  = ProductsModel::where('id',$id)
					->where('id_user',$this->admin['id'])
					->first();
			$ct  = CategoryModel::where('id_user',$this->admin['id'])->get();
		}
		$data['category']    = $ct;
		$data['product']     = $cm;
		return View::make('backend.product.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$rules = array(
			'category' => 'required',
			'name'     => 'required',
			'price'    => 'required|numeric',
			'content'  => 'required',
			'picture'  => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/products/edit/'.$id)->withErrors($valid)->withInput();
		}else{
			$picture            = Input::get('picture');
			$cm                 = ProductsModel::find($id);
			
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$cm->id_category = Input::get('category');
				$cm->name        = Input::get('name');
				$cm->price       = Input::get('price');
				$cm->note        = Input::get('content');
				$cm->picture     = ($picture?$picture:'');
				$cm->save();

				$pm = ProductsModel::find($cm->id);
				$pm->slug  = Str::slug(Input::get('name')).'-'.$cm->id;
				$pm->save();
				return Redirect::to('admin/products')->with('products','Data has been added');
			}
		}
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		$am = ProductsModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			if(!empty($am)){
				// $path       = public_path($am->picture);

				// if( is_file($path) ){
				// 	unlink($path);
				// }
				$am->delete();
				return Redirect::to('admin/products')->with('products','Data has been deleted');
			}
		}
	}

}
