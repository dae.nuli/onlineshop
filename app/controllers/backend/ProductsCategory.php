<?php

class ProductsCategory extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $limit = 10;
	public $admin;

	public function __construct()
	{
		$this->admin = Session::get('admin');
	}
	
	public function getIndex()
	{
		View::share('path','Index');
		View::share('title','Category');
		$data['limit'] = $this->limit;
		if($this->admin['level'] == 1)
		{
			$qr	= CategoryModel::orderBy('listings','asc')
						->paginate($this->limit);		
		}else{
			$qr	= CategoryModel::where('id_user',$this->admin['id'])
						->orderBy('listings','asc')
						->paginate($this->limit);
		}
		$data['category'] = $qr;
		return View::make('backend.category.index',$data);
	}

	public function postDrag(){
		$id = Input::get('id');
		if(count($id)>0){
			$lists = 1;
			foreach ($id as $key => $value) {
				$cm = CategoryModel::find($value);
				$cm->listings = $lists; 
				$cm->save(); 
				$lists = $lists + 1;
			}
		}
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		View::share('path','Create');
		View::share('title','Category');
		return View::make('backend.category.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		$rules = array(
			'name' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/product_category/create')->withErrors($valid);
		}else{
			$name = Input::get('name');
			$cm              = new CategoryModel;
			$cm->id_user     = $this->admin['id'];
			$cm->name        = $name;
			$cm->slug 		 = Str::slug($name);
			$cm->save();
			return Redirect::to('admin/product_category')->with('product_category','Data has been added');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		View::share('path','Edit');
		View::share('title','Category');

		if($this->admin['level'] == 1)
		{
			$cm = CategoryModel::find($id);
		}else{
			$cm = CategoryModel::where('id',$id)
			->where('id_user',$this->admin['id'])
			->first();
		}
		$data['category'] = $cm;
		return View::make('backend.category.edit',$data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		$rules = array(
			'name' => 'required'
			);
		$valid = Validator::make(Input::all(),$rules);
		if($valid->fails())
		{
			return Redirect::to('admin/product_category/edit/'.$id)->withErrors($valid);
		}else{
			$cm       = CategoryModel::find($id);
			if($this->admin['level'] == 1 || $cm->id_user == $this->admin['id'])
			{
				$name = Input::get('name');
				$cm->name = $name;
				$cm->slug = Str::slug($name);
				$cm->save();
				return Redirect::to('admin/product_category')->with('product_category','Data has been updated'); 
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		View::share('path','Delete');
		View::share('title','Category');

		$am = CategoryModel::find($id);
		if($am->id_user == $this->admin['id'] || $this->admin['level'] == 1){
			$pm = ProductsModel::where('id_category',$id)->count();
			if($pm > 0){
				return Redirect::to('admin/product_category')->with('product_category_alert','Data is used');
			}else{
				$am->delete();
				return Redirect::to('admin/product_category')->with('product_category','Data has been deleted');
			}
		}
	}


}
