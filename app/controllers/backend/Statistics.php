<?php

class Statistics extends BaseController {
	
	public function getIndex()
	{
		View::share('title','Statistics');
		View::share('path','index');

		$SessionYear         = Session::get('S_year_unique') ? Session::get('S_year_unique') : date('Y');
		$month               = ($SessionYear==date('Y'))?self::MonthName(date('n')):array('January','February','March','April','May','June','July','August','September','October','November','December');
		$data['year_unique'] = ($SessionYear) ? $SessionYear : date('Y');
		$data['month']       = '"'. implode('","', $month) .'"';
		
		$dateOfDetMod		   = VisitorModel::orderBy('created_at','asc')->take(1)->first();
		$Created               = ($dateOfDetMod)?date('Y', strtotime($dateOfDetMod->created_at)):date('Y');

		$data['year_range']  = range($Created,date('Y'));
		$data['visitor']     = implode(",", self::visitor($data['year_unique']));
		return View::make('backend.statistics.index',$data);
	}

	public function postIndex()
	{
		$unique = Input::get('unique');
		if($unique){	
			Session::put('S_year_unique',$unique);
		}
		return Redirect::to('admin/statistics');
	}
	public function MonthName($month)
	{
		for ($i=1; $i <=$month ; $i++) { 
			switch ($i) {
				case 1:
					$Mth[] = 'January';
				break;
				
				case 2:
					$Mth[] = 'February';
				break;
				
				case 3:
					$Mth[] = 'March';
				break;
				
				case 4:
					$Mth[] = 'April';
				break;
				
				case 5:
					$Mth[] = 'May';
				break;
				
				case 6:
					$Mth[] = 'June';
				break;
				
				case 7:
					$Mth[] = 'July';
				break;
				
				case 8:
					$Mth[] = 'August';
				break;
				
				case 9:
					$Mth[] = 'September';
				break;
				
				case 10:
					$Mth[] = 'October';
				break;
				
				case 11:
					$Mth[] = 'November';
				break;
				
				case 12:
					$Mth[] = 'December';
				break;
			}
		}
		return $Mth;
	}

	public function visitoractivity($year)
	{
		$vm = DB::table('visitor')
             ->select(DB::raw('month(created_at) as month, year(created_at) as year'))
             ->whereRaw('year(created_at) = '.$year)
             ->get();

             	if(count($vm) > 0){
			        $mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;

				    foreach ($vm as $row) {
				    	if($row->month == 1){
					        $mon['jan']++;
				    	}elseif($row->month == 2){
					        $mon['feb']++;
				    	}elseif($row->month == 3){
					        $mon['mar']++;
				    	}elseif($row->month == 4){
					        $mon['apr']++;
				    	}elseif($row->month == 5){
					        $mon['mei']++;
				    	}elseif($row->month == 6){
					        $mon['jun']++;
				    	}elseif($row->month == 7){
					        $mon['jul']++;
				    	}elseif($row->month == 8){
					        $mon['agu']++;
				    	}elseif($row->month == 9){
					        $mon['sep']++;
				    	}elseif($row->month == 10){
					        $mon['oct']++;
				    	}elseif($row->month == 11){
					        $mon['nov']++;
				    	}elseif($row->month == 12){
					        $mon['dec']++;
				    	}
				    }
				    return $mon;
				}else{
					$mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;
			        return $mon;
				}
	}

	public function visitor($year)
	{
		$vm = DB::table('visitor')
             ->select(DB::raw('month(created_at) as month, year(created_at) as year'))
             ->whereRaw('year(created_at) = '.$year)
             ->groupBy('month')
             ->groupBy('ip')
             ->get();

             	if(count($vm) > 0){
			        $mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;

				    foreach ($vm as $row) {
				    	if($row->month == 1){
					        $mon['jan']++;
				    	}elseif($row->month == 2){
					        $mon['feb']++;
				    	}elseif($row->month == 3){
					        $mon['mar']++;
				    	}elseif($row->month == 4){
					        $mon['apr']++;
				    	}elseif($row->month == 5){
					        $mon['mei']++;
				    	}elseif($row->month == 6){
					        $mon['jun']++;
				    	}elseif($row->month == 7){
					        $mon['jul']++;
				    	}elseif($row->month == 8){
					        $mon['agu']++;
				    	}elseif($row->month == 9){
					        $mon['sep']++;
				    	}elseif($row->month == 10){
					        $mon['oct']++;
				    	}elseif($row->month == 11){
					        $mon['nov']++;
				    	}elseif($row->month == 12){
					        $mon['dec']++;
				    	}
				    }
				    return $mon;
				}else{
					$mon['jan'] = 0;
			        $mon['feb'] = 0;
			        $mon['mar'] = 0;
			        $mon['apr'] = 0;
			        $mon['mei'] = 0;
			        $mon['jun'] = 0;
			        $mon['jul'] = 0;
			        $mon['agu'] = 0;
			        $mon['sep'] = 0;
			        $mon['oct'] = 0;
			        $mon['nov'] = 0;
			        $mon['dec'] = 0;
			        return $mon;
				}
	}
}