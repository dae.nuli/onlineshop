<?php
class Helper {
	public static function count_views($id)
	{
		return VisitorModel::where('id_product',$id)->count();
	}

	// public static function total_subcategory($id)
	// {
	// 	return SubCategoryModel::where('id_category',$id)->count();
	// }
// baru
	public static function TotalOfProduct($id)
	{
		return ProductsModel::where('id_category',$id)->count();
	}

	public static function SubPrefix($id)
	{
		$scm = CategoryModel::find($id);
		return $scm->prefix;
	}

	// baru

	public static function ProductTotal($idSubCategory)
	{
		return ProductsModel::where('id_subcategory',$idSubCategory)->count();
	}

	public static function total_news($id)
	{
		return NewsModel::where('id_category',$id)->count();
	}

	public static function category_name($id)
	{
		$SCM = SubCategoryModel::find($id);
		$CM  = CategoryModel::find($SCM->id_category);
		return $CM->name;
	}

	public static function selected_category($id)
	{
		$SCM = SubCategoryModel::find($id);
		$CM  = CategoryModel::find($SCM->id_category);
		return $CM->id;
	}

	

	public static function ProductPrice($code)
	{
		$pm = ProductsModel::where('code',$code)->first();
		return $pm->price;
	}

	public static function findCodebyID($ID)
	{
		$pm = ProductsModel::find($ID);
		return $pm->code;
	}

	public static function TotalPayment($idOrder)
	{
		$pay = OrdersPaymentModel::where('id_order',$idOrder)->count();
		if($pay){
			$OPM = OrdersPaymentModel::where('id_order',$idOrder)->get();
			foreach ($OPM as $key => $value) {
				$total[] = $value->amount;
			}
			$amount = array_sum($total);
		}else{
			$amount = 0; 
		}
		return $amount;
	}

	public static function CheckPayment($idOrder)
	{
		$om = OrdersModel::find($idOrder);
		$pay = OrdersPaymentModel::where('id_order',$idOrder)->count();
		if($pay){
			$OPM = OrdersPaymentModel::where('id_order',$idOrder)->get();
			foreach ($OPM as $key => $value) {
				$total[] = $value->amount;
			}
			$amount = array_sum($total);
		}else{
			$amount = 0; 
		}

		if(!empty($amount)){
			if($om->total == $amount || $amount > $om->total ){
				return '<span class="label label-success">Paid</span>';
			}else{
				return '<span class="label label-warning">Not Paid</span>';
			}
		}else{
			return '<span class="label label-warning">Not Paid</span>';
		}
	}

	public static function CheckPaymentStatus($idOrder)
	{
		$om = OrdersModel::find($idOrder);
		$pay = OrdersPaymentModel::where('id_order',$idOrder)->count();
		if($pay){
			$OPM = OrdersPaymentModel::where('id_order',$idOrder)->get();
			foreach ($OPM as $key => $value) {
				$total[] = $value->amount;
			}
			$amount = array_sum($total);
		}else{
			$amount = 0; 
		}

		if(!empty($amount)){
			if($om->total == $amount || $amount > $om->total ){
				return '<span class="label label-success">Lunas</span>';
			}else{
				return '<span class="label label-warning">Belum Lunas</span>';
			}
		}else{
			return '<span class="label label-warning">Belum Lunas</span>';
		}
	}

	public static function CheckOrder($idOrder)
	{
		$om = OrdersModel::find($idOrder);
		$start_date  = strtotime($om->start_create);
		$finish_date = strtotime($om->finish_create);
		$now         = strtotime(date('Y-m-d'));
		if(!empty($start_date) || !empty($finish_date)){
			if($now >= $start_date && $now < $finish_date)
			{
				return '<span class="label label-info">On Process</span>';
			}else if($now >= $finish_date){
				return '<span class="label label-success">Finish</span>';
			}else{
				return '<span class="label label-danger">New</span>';
			}
		}else{
			return '<span class="label label-danger">New</span>';
		}
	}

	public static function OrderTotal($idClient)
	{
		return OrdersModel::where('id_client',$idClient)->count();
	}

	public static function GroupTotal($idGroup)
	{
		return UserModel::where('level',$idGroup)->count();
	}

	public static function CheckGroupName($idGroup)
	{
		return UsersGroupModel::find($idGroup)->group_name;
	}

	public static function FindCategory($CodeProduct)
	{
		$PM  = ProductsModel::where('code',$CodeProduct)->first();
		$SCM = SubCategoryModel::find($PM->id_subcategory);
		$CM  = CategoryModel::find($SCM->id_category);
		return $CM->name;
	}

	public static function FindSubCategory($CodeProduct)
	{
		$PM  = ProductsModel::where('code',$CodeProduct)->first();
		$SCM = SubCategoryModel::find($PM->id_subcategory);
		return $SCM->name;
	}

	public static function collectedPayment()
	{
		$PM = PaymentModel::all();
		$total[] = 0;
		foreach ($PM as $key => $row) {
			$total[] = $row->amount;
		}
		return array_sum($total);
	}

	public static function CheckStatusPayment($idOrder)
	{
		$OPM = OrdersPaymentModel::where('id_order',$idOrder)->get();
		foreach ($OPM as $key => $row) {
			$total[] = $row->amount;
		}
		$grandTotal = array_sum($total);
		$OM = OrdersModel::find($idOrder);
		
		if(!empty($OM)){
			if($OM->total == $grandTotal){
				$OM->status = 1;
			}else{
				$OM->status = 0;
			}
			$OM->save();
		}

	}

	public static function ReportGrandTotal($from,$to)
	{
		$total[] =0;
		if(!empty($from) && !empty($to)){
		$OM = OrdersModel::where('status',1)
				->orderBy('created_at','asc')
				->whereBetween('created_at',array($from.' 00:00:00',$to.' 23:59:59'))->get();
				foreach ($OM as $key => $row) {
					$total[] = $row->total;
				}
				return array_sum($total);
		}else{
		$OM = OrdersModel::where('status',1)
				->orderBy('created_at','asc')
				->get();
				foreach ($OM as $key => $row) {
					$total[] = $row->total;
				}
				return array_sum($total);
		}
	}

	public static function CategoryMenu()
	{
		return CategoryModel::orderBy('listings','asc')->take(7)->get();
	}
	// Front End




}