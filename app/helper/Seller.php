<?php
class Seller {
	public static function GetPicture($CodeProduct)
	{
		$PM = ProductsModel::where('code',$CodeProduct)->first();
		return $PM->picture;
	}

	public static function GetViewer($CodeProduct)
	{
		$PM = ProductsModel::where('code',$CodeProduct)->first();
		return VisitorModel::where('id_article',$PM->id)->where('type',1)->count();
	}

	public static function GetUserName($CodeProduct)
	{
		$PM = ProductsModel::where('code',$CodeProduct)->first();
		$UM = UserModel::find($PM->id_user);
		return $UM->name;
	}

	public static function GetModified($CodeProduct)
	{
		$PM = ProductsModel::where('code',$CodeProduct)->first();
		return $PM->updated_at;
	}
}