<?php
class FrontEnd {
	public static function about()
	{
		return AboutModel::find(1);
	}

	public static function findProductByCode($codeProduct)
	{
		return ProductsModel::where('code',$codeProduct)->first();
	}

	public static function LeftMenu()
	{
		$cm = CategoryModel::orderBy('id','desc')->get();
		foreach ($cm as $key => $row) {
			echo '
			    <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <a id="'.self::ActiveCategory($row->slug).'" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$row->id.'" '.self::CollapseTrueFalse($row->slug).' aria-controls="collapse'.$row->id.'">
                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                        '.$row->name.'
                      </a>
                    </h4>
                  </div>
                ';
			$scm = SubCategoryModel::where('id_category',$row->id)->orderBy('id','desc')->get();
			if(count($scm)>0){
				echo '<div id="collapse'.$row->id.'" class="panel-collapse collapse'.self::CollapseIn($row->slug).'" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <ul class="sub-category">';
						foreach ($scm as $key => $value) {
		                        echo '<li><a id="'.self::ActiveSubCategory($value->slug).'" href="'.URL::to('product/'.$row->slug.'/'.$value->slug).'">'.$value->name.'</a></li>';
						}
						echo '
                      </ul>
                    </div>
                  </div>
                ';
			}
			echo '</div>';
		}
		// return $SideMenu;
	}

	public static function BankAccount()
	{
		$bm = BankModel::orderBy('id','desc')->get();
		foreach ($bm as $key => $value) {
			echo '<div class="bank-item">
				    <img src="'.asset($value->picture).'">
				    <h6>'.$value->name.' : '.$value->account.'</h6>
				  </div>';
		}
	}

	public static function Testimony()
	{
		$tm = TestimonyModel::where('status',1)->orderBy('id','desc')->get();
		foreach ($tm as $key => $row) {
            echo '<div><img class="owl-lazy group2" href="'.asset($row->picture).'" data-src="'.asset($row->picture).'" alt="Owl Image"></div>';
		}
	}

	public static function OrderTotal($code,$qty)
	{
		$PM    = ProductsModel::where('code',$code)->first();
		$total = $PM->price*$qty;
		return $total;
	}

	public static function OrderTotalArray($code,$qty)
	{
		foreach ($code as $key => $value) {
			$PM    = ProductsModel::where('code',$value)->first();
			$total[] = $PM->price*$qty[$key];
		}
		return array_sum($total);
	}

	public static function ItemPrice($code)
	{
		$PM = ProductsModel::where('code',$code)->first();
		return $PM->price;
	}

	public static function CreateURLByCode($code)
	{
		$PM  = ProductsModel::where('code',$code)->first();
		$SCM = SubCategoryModel::find($PM->id_subcategory);
		$CM  = CategoryModel::find($SCM->id_category);
		return $URL = 'product/'.$CM->slug.'/'.$SCM->slug.'?id='.$code;
	}

	public static function CreateURLByIdSubCategory($idSubCategory)
	{
		$SCM = SubCategoryModel::find($idSubCategory);
		$CM  = CategoryModel::find($SCM->id_category);
		return $URL = 'product/'.$CM->slug.'/'.$SCM->slug;
	}

	public static function AddVisitor($IP,$id)
	{
		$VM             = new VisitorModel;
		$VM->ip         = $IP;
		$VM->id_product = ($id)?$id:'';
		$VM->save();
	}

	public static function VisitorToday()
	{
		$VM = VisitorModel::where('created_at','LIKE',date('Y-m-d').'%')->groupBy('ip')->get();
		return count($VM);
	}

	public static function AllVisitor()
	{
		$VM = VisitorModel::groupBy('ip')->get();
		return count($VM);	
	}

	public static function CollapseTrueFalse($slug)
	{
		$URI1 = Request::segment(1);
		$URI2 = Request::segment(2);
		$URI3 = Request::segment(3);
		if($URI1=='product')
		{
			$CM = CategoryModel::where('slug',$URI2)->first();
			if($CM->slug == $slug)
			{
				return 'aria-expanded="true"';
			}else{
				return 'aria-expanded="false"';
			}
		}else{
			return 'aria-expanded="false"';
		}
	}

	public static function CollapseIn($slug)
	{
		$URI1 = Request::segment(1);
		$URI2 = Request::segment(2);
		$URI3 = Request::segment(3);
		if($URI1=='product')
		{
			$CM = CategoryModel::where('slug',$URI2)->first();
			if($CM->slug == $slug)
			{
				return ' in';
			}else{
				return '';
			}
		}else{
			return '';
		}
	}

	public static function ActiveCategory($slug)
	{
		$URI1 = Request::segment(1);
		$URI2 = Request::segment(2);
		$URI3 = Request::segment(3);
		if($URI1=='product')
		{
			$CM = CategoryModel::where('slug',$URI2)->first();
			if($CM->slug == $slug)
			{
				return 'merah';
			}else{
				return '';
			}
		}else{
			return '';
		}	
	}

	public static function ActiveSubCategory($slug)
	{
		$URI1 = Request::segment(1);
		$URI2 = Request::segment(2);
		$URI3 = Request::segment(3);
		if($URI1=='product' && !empty($URI3))
		{
			$CM = SubCategoryModel::where('slug',$URI3)->first();
			if($CM->slug == $slug)
			{
				return 'merah';
			}else{
				return '';
			}
		}else{
			return '';
		}	
	}

	public static function ArticleComment($id)
	{
		return NewsCommentModel::where('id_article',$id)->count();
	}

	public static function findIdBySlug($slug)
	{
		$NM = NewsModel::where('slug',$slug)->first();
		return $NM->id;
	}

	public static function views($id)
	{
		return VisitorModel::where('id_product',$id)->count();
	}
	
	public static function keywords()
	{
		return AboutModel::find(1)->web_keywords;
	}

	public static function FindSubCategory($idSub)
	{
		return SubCategoryModel::find($idSub)->name;
	}
}
