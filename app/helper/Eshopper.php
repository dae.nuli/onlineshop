<?php
class Eshopper {
	public static function about()
	{
		return AboutModel::find(1);
	}
	
	public static function LeftMenu()
	{
		$cm = CategoryModel::orderBy('id','desc')->get();
		foreach ($cm as $key => $row) {
			echo '<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"><a href="'.URL::to('product/'.$row->slug).'">'.$row->name.'</a></h4>
					</div>
				</div>';
		}
	}

	public static function OtherProducts($id,$idCat)
	{
		$otherProducts          = ProductsModel::where('id','!=',$id)->where('id_category',$idCat)->orderBy('id','desc')->take(8)->get();
		$product_active    = array();
		$product_no_active = array();
		if(count($otherProducts)>0){
			foreach ($otherProducts as $key => $row) {
				if($key<=3){
					$product_active[] 	 = $row->id;
				}else{
					$product_no_active[] = $row->id;
				}
				// $product[] = FrontEnd::findProductByCode($row->code_product);
			}
			// $data['otherProducts'] = $product;
		}

		if(count($product_active) >= 1 && count($product_active) <=4):
		echo '<div class="item active">';
				foreach($product_active as $act){
 				$pro_act = self::findProductByCode($act);
				echo '<div class="col-sm-3">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<a href="'.URL::to(self::URLbyCode($pro_act->id)).'"><img src="'.asset($pro_act->picture).'" alt="'.$pro_act->code.'" /></a>
							</div>
						</div>
						<p><a href="'.URL::to(self::URLbyCode($pro_act->id)).'" class="code_link">'.$pro_act->name.'</a></p>
						<h4>Rp. '.number_format($pro_act->price,0,",",".").'</h4>
					</div>
				</div>';
				}
		echo '</div>';
		endif;

		if(count($product_no_active) >= 1 && count($product_no_active) <= 4):
		echo '<div class="item">';
				foreach($product_no_active as $non){
				$pro_non = self::findProductByCode($non);
				echo '<div class="col-sm-3">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<a href="'.URL::to(self::URLbyCode($pro_non->id)).'"><img src="'.asset($pro_non->picture).'" alt="'.$pro_non->code.'" /></a>
							</div>
						</div>
						<p><a href="'.URL::to(self::URLbyCode($pro_non->id)).'" class="code_link">'.$pro_non->name.'</a></p>
						<h4>Rp. '.number_format($pro_act->price,0,",",".").'</h4>
					</div>
				</div>';
				}
		echo '</div>';
		endif;


	}

	public static function findProductByCode($codeProduct)
	{
		return ProductsModel::find($codeProduct);
	}

	public static function URLbyCode($id)
	{
		$PM  = ProductsModel::find($id);
		$CM  = CategoryModel::find($PM->id_category);
		return $URL = $CM->slug.'/'.$PM->slug;
	}
}