$(function() {
	$(document).on('click','.delete',function(){
		return confirm('Are you sure want to delete ?');
	});

    $(document).on('click','.restore',function(){
        return confirm('Are you sure want to restore ?');
    });

    $(document).on('click','.actived',function(){
        return confirm('Are you sure want to active ?');
    });

    $(document).on('click','.noactived',function(){
        return confirm('Are you sure want to non active ?');
    });

    $(document).on('click','.published',function(){
        return confirm('Are you sure want to publish ?');
    });

    $(document).on('click','.drafted',function(){
        return confirm('Are you sure want to draft ?');
    });

	$('.filter').on('click',function (e){
        e.preventDefault();
	        if($(this).data('method')==='filter'){
		        $('#compose-modal').modal('show');
	        }
    });

    $('.info-client').on('click',function (e){
        var idClient = $(this).data('idclient');
        $('#open-modal').load('/admin/orders/modal-client-info/'+idClient);
    });

    $('.message-detail').on('click',function (e){
        var idMessage = $(this).data('idmessage');
        $('#open-modal').load('/admin/messages/detail/'+idMessage);
    });

    $('.add-orders').on('click',function (e){
        $('#open-modal').load('/admin/orders/modal-add-orders');
    });

    $('.add-item').on('click',function (e){
        // e.preventDefault();
        var id_orders = $(this).data('idorder');
        $('#open-modal').load('/admin/orders/modal-add-item/'+id_orders);
    });

    $('.edit-payment').on('click',function (e){
        // e.preventDefault();
        var id_orders = $(this).data('idorder');
        var id_payment = $(this).data('idpayment');
        $('#open-modal').load('/admin/orders/modal-edit-payment/'+id_orders+'/'+id_payment);
    });

    $('.add-payment').on('click',function (e){
        // e.preventDefault();
        var id_orders = $(this).data('idorder');
        $('#open-modal').load('/admin/orders/modal-add-payment/'+id_orders);
    });

    $('.detail-client').on('click',function (e){
        // e.preventDefault();
        var id_client = $(this).data('idclient');
        $('#open-modal').load('/admin/clients/modal-detail-client/'+id_client);
    });

    $('.detail-user').on('click',function (e){
        // e.preventDefault();
        var id_user = $(this).data('iduser');
        $('#open-modal').load('/admin/users/modal-detail-user/'+id_user);
    });

    $('.deleted-staff').on('click',function (e){
        $('#open-modal').load('/admin/users/modal-deleted-user');
    });

    $(document).on('click','.waiting',function(){
        $('.waiting').addClass('disabled');
        // $('.waiting').html('Waiting ...');
    })
});