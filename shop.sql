-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 24, 2020 at 04:58 PM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `web_keywords` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `gplus` varchar(100) NOT NULL,
  `bbm` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `lat` varchar(50) NOT NULL,
  `lng` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `name`, `email`, `web_keywords`, `address`, `facebook`, `twitter`, `instagram`, `gplus`, `bbm`, `phone`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 'Online Shop', 'giarsyani.nuli@gmail.com', 'giarsyaninuli, keren', 'Jl. Ringroad', 'giarsyaninuli', 'giarsyaninuli', 'giarsyaninuli', '104894969154772273309', '12we32', '081915804771', '-8.757573', '116.276647', '2015-09-15 17:53:27', '2015-09-15 17:53:27');

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_controller` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `id_group`, `id_controller`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 8, '2015-03-24 09:29:04', '2015-03-24 09:22:59', '2015-03-24 09:29:04'),
(2, 2, 8, '2015-03-24 09:29:14', '2015-03-24 09:29:04', '2015-03-24 09:29:14'),
(3, 2, 9, '2015-03-24 09:29:14', '2015-03-24 09:29:04', '2015-03-24 09:29:14'),
(4, 2, 17, '2015-03-24 09:29:14', '2015-03-24 09:29:04', '2015-03-24 09:29:14'),
(5, 2, 4, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(6, 2, 5, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(7, 2, 6, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(8, 2, 7, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(9, 2, 8, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(10, 2, 9, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(11, 2, 17, '2015-03-24 09:34:53', '2015-03-24 09:29:14', '2015-03-24 09:34:53'),
(12, 2, 4, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(13, 2, 5, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(14, 2, 6, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(15, 2, 7, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(16, 2, 8, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(17, 2, 9, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(18, 2, 14, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(19, 2, 17, '2015-03-24 09:38:33', '2015-03-24 09:34:53', '2015-03-24 09:38:33'),
(20, 2, 4, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(21, 2, 5, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(22, 2, 6, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(23, 2, 7, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(24, 2, 8, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(25, 2, 9, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(26, 2, 17, '2015-03-24 09:41:08', '2015-03-24 09:38:33', '2015-03-24 09:41:08'),
(27, 2, 4, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(28, 2, 5, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(29, 2, 6, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(30, 2, 7, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(31, 2, 8, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(32, 2, 9, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(33, 2, 13, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(34, 2, 14, '2015-03-24 09:41:22', '2015-03-24 09:41:08', '2015-03-24 09:41:22'),
(35, 2, 15, '2015-03-24 09:41:22', '2015-03-24 09:41:09', '2015-03-24 09:41:22'),
(36, 2, 17, '2015-03-24 09:41:22', '2015-03-24 09:41:09', '2015-03-24 09:41:22'),
(37, 2, 8, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(38, 2, 9, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(39, 2, 13, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(40, 2, 14, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(41, 2, 15, '2015-04-14 08:25:40', '2015-03-24 09:41:22', '2015-04-14 08:25:40'),
(42, 2, 17, '2015-04-14 08:25:40', '2015-03-24 09:41:23', '2015-04-14 08:25:40'),
(43, 2, 8, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(44, 2, 9, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(45, 2, 13, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(46, 2, 14, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(47, 2, 15, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(48, 2, 17, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(49, 2, 22, '2015-04-14 08:27:02', '2015-04-14 08:25:40', '2015-04-14 08:27:02'),
(50, 2, 23, '2015-04-14 08:27:02', '2015-04-14 08:25:41', '2015-04-14 08:27:02'),
(51, 2, 24, '2015-04-14 08:27:02', '2015-04-14 08:25:41', '2015-04-14 08:27:02'),
(52, 2, 25, '2015-04-14 08:27:02', '2015-04-14 08:25:41', '2015-04-14 08:27:02'),
(53, 2, 8, '2015-04-18 02:14:27', '2015-04-14 08:27:02', '2015-04-18 02:14:27'),
(54, 2, 9, '2015-04-18 02:14:27', '2015-04-14 08:27:02', '2015-04-18 02:14:27'),
(55, 2, 10, '2015-04-18 02:14:27', '2015-04-14 08:27:02', '2015-04-18 02:14:27'),
(56, 2, 13, '2015-04-18 02:14:27', '2015-04-14 08:27:02', '2015-04-18 02:14:27'),
(57, 2, 14, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(58, 2, 15, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(59, 2, 17, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(60, 2, 21, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(61, 2, 22, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(62, 2, 23, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(63, 2, 24, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(64, 2, 25, '2015-04-18 02:14:27', '2015-04-14 08:27:03', '2015-04-18 02:14:27'),
(65, 2, 8, '2015-10-18 06:57:19', '2015-04-18 02:14:27', '2015-10-18 06:57:19'),
(66, 2, 9, '2015-10-18 06:57:23', '2015-04-18 02:14:27', '2015-10-18 06:57:23'),
(67, 2, 10, '2015-10-18 06:57:03', '2015-04-18 02:14:27', '2015-10-18 06:57:03'),
(68, 2, 13, '2015-10-18 06:57:37', '2015-04-18 02:14:27', '2015-10-18 06:57:37'),
(69, 2, 14, '2015-10-18 06:57:36', '2015-04-18 02:14:27', '2015-10-18 06:57:36'),
(70, 2, 15, '2015-10-18 06:57:36', '2015-04-18 02:14:27', '2015-10-18 06:57:36'),
(71, 2, 17, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(72, 2, 21, '2015-10-18 06:57:12', '2015-04-18 02:14:27', '2015-10-18 06:57:12'),
(73, 2, 22, '2015-10-18 06:57:41', '2015-04-18 02:14:27', '2015-10-18 06:57:41'),
(74, 2, 23, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(75, 2, 24, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(76, 2, 25, NULL, '2015-04-18 02:14:27', '2015-04-18 02:14:27'),
(77, 2, 26, '2015-10-18 06:58:03', '2015-04-18 02:14:27', '2015-10-18 06:58:03');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `account` varchar(100) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `name`, `account`, `picture`, `created_at`, `updated_at`) VALUES
(1, 'bni', '2389472346286', '/fileman/Uploads/bni.png', '2015-04-07 06:24:55', '2015-04-07 06:29:57'),
(2, 'bri', '2314235435323', '/fileman/Uploads/bri.gif', '2015-04-07 06:27:26', '2015-04-07 06:30:31'),
(4, 'mandiri', '932-846-27', '/fileman/Uploads/mandiri.png', '2015-04-07 06:30:43', '2015-04-07 06:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `position` varchar(10) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `slug`, `picture`, `content`, `status`, `position`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'Diskon 50 % untuk pembelian bulan Januari', 'a', '/fileman/Uploads/Images/spesial.png', '<p><span style=\"font-family: Arial, Helvetica, sans; line-height: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum leo sit amet luctus suscipit. Donec lacinia neque quis sapien dictum, vel tincidunt lacus sollicitudin. Suspendisse eu quam eros. Nam urna arcu, dictum in gravida vel, imperdiet sed turpis. Etiam felis metus, auctor a efficitur eu, dictum vel leo. Etiam luctus velit ut erat placerat, quis sodales libero aliquet. Ut vulputate, odio eu ultrices molestie, urna tellus maximus dolor, ac luctus dolor augue sit amet ante. Nullam odio nisi, aliquam quis aliquam tincidunt, interdum eu eros.</span></p>', '1', '0', 0, '2015-04-10 02:38:27', '2015-09-15 18:21:10'),
(2, 'Diskon 50 % untuk pembelian bulan desember', 'b', '/fileman/Uploads/Images/995978_406909419421201_199597060.jpg', '<p><span style=\"font-family: Arial, Helvetica, sans; line-height: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum leo sit amet luctus suscipit. Donec lacinia neque quis sapien dictum, vel tincidunt lacus sollicitudin. Suspendisse eu quam eros. Nam urna arcu, dictum in gravida vel, imperdiet sed turpis. Etiam felis metus, auctor a efficitur eu, dictum vel leo. Etiam luctus velit ut erat placerat, quis sodales libero aliquet. Ut vulputate, odio eu ultrices molestie, urna tellus maximus dolor, ac luctus dolor augue sit amet ante. Nullam odio nisi, aliquam quis aliquam tincidunt, interdum eu eros.</span></p>', '1', '0', 0, '2015-04-10 02:48:33', '2015-09-15 18:20:49'),
(3, 'Diskon 50 % untuk pembelian bulan Agustus', 'c', '/fileman/Uploads/Images/spesial.png', '<p style=\"text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum leo sit amet luctus suscipit. Donec lacinia neque quis sapien dictum, vel tincidunt lacus sollicitudin. Suspendisse eu quam eros. Nam urna arcu, dictum in gravida vel, imperdiet sed turpis. Etiam felis metus, auctor a efficitur eu, dictum vel leo. Etiam luctus velit ut erat placerat, quis sodales libero aliquet. Ut vulputate, odio eu ultrices molestie, urna tellus maximus dolor, ac luctus dolor augue sit amet ante. Nullam odio nisi, aliquam quis aliquam tincidunt, interdum eu eros.</p>\r\n<p style=\"text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;\">Vivamus interdum tellus vitae orci convallis, eu lacinia risus tempor. Maecenas vulputate faucibus commodo. Integer congue quam in eleifend faucibus. Aliquam vitae elementum velit. Curabitur posuere condimentum vestibulum. Cras eget porttitor magna. In hac habitasse platea dictumst. Mauris malesuada finibus sollicitudin. Vestibulum vitae justo sapien. Proin consectetur risus in ipsum ornare sollicitudin a vitae enim. Cras eros nunc, fermentum at fringilla a, pulvinar vitae sapien. Sed semper rutrum ante, ac sodales risus scelerisque ut. Maecenas pretium sed tellus a auctor. Etiam congue, ipsum vitae malesuada vehicula, ipsum quam efficitur massa, a dictum massa dui et nulla.</p>', '1', '-8', 0, '2015-04-10 02:56:18', '2015-09-15 18:19:53');

-- --------------------------------------------------------

--
-- Table structure for table `controllers`
--

CREATE TABLE `controllers` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `controllers`
--

INSERT INTO `controllers` (`id`, `id_parent`, `name`, `url`, `description`, `icon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Blog', NULL, 'Blog Parent', 'fa fa-file-o', NULL, '2015-03-24 08:02:15', '2015-03-24 08:02:15'),
(2, 1, 'Category', 'news_category', 'Blog Category', '', NULL, '2015-03-24 08:03:03', '2015-03-24 08:03:03'),
(3, 1, 'List', NULL, 'Blog List', '', NULL, '2015-03-24 08:05:39', '2015-03-24 08:05:39'),
(4, NULL, 'Products', NULL, 'Products', 'fa fa-list-alt', '2015-10-18 06:57:17', '2015-03-24 08:08:53', '2015-10-18 06:57:17'),
(5, 4, 'Category', 'product_category', 'Product Category', '', '2015-10-18 06:57:16', '2015-03-24 08:11:07', '2015-10-18 06:57:16'),
(6, 4, 'Sub Category', 'product_subcategory', 'Product Sub Category', '', '2015-10-18 06:57:16', '2015-03-24 08:12:13', '2015-10-18 06:57:16'),
(7, 4, 'List', 'products', 'Product List', '', '2015-10-18 06:57:16', '2015-03-24 08:12:36', '2015-10-18 06:57:16'),
(8, NULL, 'Orders', 'orders', 'Orders List', 'fa fa-shopping-cart', '2015-10-18 06:57:20', '2015-03-24 08:13:06', '2015-10-18 06:57:20'),
(9, NULL, 'Clients', 'clients', 'Clients List', 'fa fa-users', '2015-10-18 06:57:23', '2015-03-24 08:13:32', '2015-10-18 06:57:23'),
(10, NULL, 'Best Seller', 'best', 'Best Seller List', 'fa fa-star', '2015-10-18 06:57:03', '2015-03-24 08:14:07', '2015-10-18 06:57:03'),
(11, NULL, 'Testimony', 'testimony', 'Testimony List', 'fa fa-comments', '2015-10-18 06:57:28', '2015-03-24 08:14:49', '2015-10-18 06:57:28'),
(12, NULL, 'Gallery', 'gallery', 'Gallery List', 'fa fa-picture-o', '2015-10-18 06:57:34', '2015-03-24 08:15:20', '2015-10-18 06:57:34'),
(13, NULL, 'Management Staff', NULL, 'Management Staff Menu', 'fa fa-user', '2015-10-18 06:57:37', '2015-03-24 09:30:57', '2015-10-18 06:57:37'),
(14, 13, 'List', 'users', 'Staff List', '', '2015-10-18 06:57:37', '2015-03-24 09:31:27', '2015-10-18 06:57:37'),
(15, 13, 'Deleted Staff', 'deleted-staff', 'Deleted Staff List', '', '2015-10-18 06:57:37', '2015-03-24 09:31:53', '2015-10-18 06:57:37'),
(17, NULL, 'About', 'about', 'About Menu', 'fa fa-info-circle', NULL, '2015-03-24 08:18:46', '2015-03-24 08:53:07'),
(21, NULL, 'Reports', 'report', 'Reports Menu', 'fa fa-bar-chart-o', '2015-10-18 06:57:12', '2015-03-24 09:33:59', '2015-10-18 06:57:12'),
(22, NULL, 'Bank', 'bank', 'Bank is menu for input bank account', 'fa fa-usd', '2015-10-18 06:57:41', '2015-04-14 08:04:07', '2015-10-18 06:57:41'),
(23, NULL, 'How To Buy', 'how', 'is menu for description some step to buy product', 'fa fa-question-circle', NULL, '2015-04-14 08:05:37', '2015-04-14 08:05:37'),
(24, NULL, 'Banner', 'banner', 'is menu to set banner content', 'fa fa-desktop', NULL, '2015-04-14 08:06:17', '2015-04-14 08:06:17'),
(25, NULL, 'Statistics', 'statistics', 'is menu to view visitor activity', 'fa fa-signal', NULL, '2015-04-14 08:08:07', '2015-04-14 08:08:07'),
(26, NULL, 'Messages', 'messages', 'Is all about contact ', 'fa fa-envelope', '2015-10-18 06:58:03', '2015-04-18 02:14:15', '2015-10-18 06:58:03');

-- --------------------------------------------------------

--
-- Table structure for table `how_to_buy`
--

CREATE TABLE `how_to_buy` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `how_to_buy`
--

INSERT INTO `how_to_buy` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/fileman/Uploads/how.png\" alt=\"\" width=\"600\" height=\"267\" /></p>\r\n<p style=\"text-align: center;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p style=\"text-align: center;\"><img src=\"/fileman/Uploads/bni.png\" alt=\"\" width=\"200\" height=\"65\" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"/fileman/Uploads/bri.gif\" alt=\"\" width=\"116\" height=\"61\" /></p>\r\n<p style=\"text-align: center;\"><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2015-09-15 17:59:07', '2015-09-15 17:59:07');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `active` enum('1','0') DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `handphone` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `name`, `email`, `handphone`, `content`, `created_at`, `updated_at`) VALUES
(2, 'gansing', 'dae.nuli@gmail.com', '92830382', 'res', '2015-04-12 07:59:53', '2015-04-12 07:59:53'),
(3, 'Uye', 'ef.ridwan4896@gmail.com', '081915804771', 'ganteng', '2015-04-12 08:00:30', '2015-04-12 08:00:30'),
(4, 'gayus', 'tambunan@gmail.com', '74983', 'tmabun', '2015-04-12 08:00:56', '2015-04-12 08:00:56'),
(5, 'nama lengkap', 'email@gmail.com', '081915804771', 'ini pesan anak ganteng se duania\r\n', '2015-04-16 02:43:10', '2015-04-16 02:43:10');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `content` text,
  `id_user` int(11) DEFAULT NULL,
  `picture` varchar(30) NOT NULL,
  `publish` enum('1','0') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `slug`, `title`, `id_category`, `content`, `id_user`, `picture`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'perkembangan-teknologi-informasi-1', 'Perkembangan teknologi informasi', 2, '<p><em>dae</em> <strong>ganteng</strong></p>', 1, '8zjQ4dgVlz.jpg', '1', '2015-03-06 20:44:19', '2015-03-06 20:44:19'),
(2, 'pentingnya-menjaga-silaturahmi-2', 'pentingnya menjaga silaturahmi', 2, '<p>SETIAP manusia pasti tidak bisa hidup seorang diri. Mengapa? Karena manusia merupakan makhluk sosial yang membutuhkan orang lain. Oleh karena itu, penting bagi setiap manusia untuk menjalin silaturahmi yang baik dengan orang lain. Jangan kita mengikuti nafsu semata. Yang akibatnya akan memutuskan tali silaturahmi.</p>\r\n<p>Silaturahmi bukanlah murni adat istiadat, namun ia merupakan bagian dari syariat. Amat bervariasi cara agama kita dalam memotivasi umatnya untuk memperhatikan silaturahmi. Terkadang dengan bentuk perintah secara gamblang, janji ganjaran menarik, atau juga dengan cara ancaman bagi mereka yang tidak menjalankannya.</p>\r\n<p>Allah Ta&rsquo;ala memerintahkan berbuat baik pada kaum kerabat, &ldquo;Sembahlah Allah dan janganlah kalian mempersekutukan-Nya dengan sesuatu apa pun. Serta berbuat baiklah kepada kedua orangtua, karib-kerabat, anak-anak yatim, orang-orang miskin, tetangga dekat dan tetangga jauh, teman, musafir dan hamba sahaya yang kalian miliki. Sungguh Allah tidak menyukai orang yang sombong dan membanggakan diri,&rdquo; (QS. An-Nisa&rsquo;: 36).</p>\r\n<p>Rasulullah shallallahu &rsquo;alaihi wa sallam menerangkan bahwa silaturahmi merupakan pertanda keimanan seorang hamba kepada Allah dan hari akhir, &ldquo;Barang siapa beriman kepada Allah dan hari akhir, hendaklah ia bersilaturahmi,&rdquo; (HR. Bukhari dari Abu Hurairah).</p>\r\n<p>Beliau juga menjanjikan bahwa di antara buah dari silaturahmi adalah keluasan rezeki dan umur yang panjang, &ldquo;Barang siapa menginginkan untuk diluaskan rezekinya serta diundur ajalnya; hendaklah ia bersilaturahmi,&rdquo; (HR. Bukhari dan Muslim dari Anas bin Malik).</p>\r\n<p><br /><img src=\"../../../fileman/Uploads/dreamland_beach_13603_1920x1200_ - Copy 1.jpg\" alt=\"\" width=\"600\" height=\"337\" />Dalam hal bersilaturahmi misalnya kita bisa mencontoh semut dan lebah. Semut binatang kecil pemakan gula tapi tidak pernah sakit gula (diabetes). Resepnya, pertama karena semut senang bersilaturahmi. Tengoklah setiap berpapasan antara sesama semut sejenis mereka saling &ldquo;bersalaman&rdquo; yang terlihat dari kedua kepalanya saling ketemu. Kedua, bila seekor semut menemukan rezeki, mereka tidak mau makan sendiri tapi memberi tahu semut-semut lainnya. Setelah berkumpul, baru makanan itu mereka bawa kesatu tempat dan dinikmati bersama. Demikian juga lebah. &ldquo;Lebah sangat disiplin dan mengenal pembagian kerja yang sangat baik. Sarangnya dibangun berbentuk segi enam, yang telah terbukti sangat ekonomis dan kuat dibandingkan bila segi empat atau lima.&rdquo; Antara tulis Ir. Permadi Alibasyah dalam bukunya &ldquo;Bahan Renungan Kalbu.&rdquo; Menurut penyelidikan setiap sarang lebah dihuni oleh kurang lebih 90.000 ekor lebah. Karena masing-masing mentaati aturan mereka bisa hidup rukun dan tidak pernah terjadi perkelahian.</p>\r\n<p>Dari firman Allah, hadits Rasul SAW dan beberapa contoh dalam kehidupan ini, jelas bahwa silaturahmi sangat bermanfaat bagi manusia. Maka dari itu, kita sebagai makhluk yang diberi akal, gunakanlah akal ini dengan baik. Karena akal merupakan anugerah yang telah diberikan oleh Allah SWT. Semut dan lebah saja dapat hidup rukun, walaupun mereka tidak memiliki akal. Jadi, kita juga harus bisa seperti hewan tersebut, yang menjalin silaturahmi dengan baik. Serta peduli akan sesama</p>', 1, 'AQVo1JyfgL.jpg', '1', '2015-03-10 20:12:35', '2015-04-10 01:54:41'),
(3, 'modeling-vs-encoding-for-the-semantic-web-3', 'Modeling vs Encoding for the Semantic Web', 2, '<p>Abstract. The Semantic Web emphasizes encoding over modeling. It is built on the premise that ontology engineers can say&nbsp;something useful about the semantics of vocabularies by expressing themselves in an encoding language for automated rea-soning. This assumption has never been systematically tested and the shortage of documented successful applications of Se-mantic Web ontologies suggests it is wrong. Rather than blaming OWL and its expressiveness (in whatever flavor) for thisstate of affairs, we should improve the modeling techniques with which OWL code is produced. I propose, therefore, to sepa-rate the concern of modeling from that of encoding, as it is customary for database or user interface design. Modeling seman-tics is a design task, encoding it is an implementation. Ontology research, for applications in the Semantic Web or elsewhere,should produce languages for both. Ontology modeling languages primarily support ontological distinctions and secondarily(where possible and necessary) translation to encoding languages.</p>', 1, 'SwXQDaXZif.jpg', '1', '2015-03-10 20:17:07', '2015-04-10 01:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE `news_category` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_category`
--

INSERT INTO `news_category` (`id`, `id_user`, `name`, `created_at`, `updated_at`) VALUES
(2, 1, 'test', '2015-03-06 20:31:17', '2015-10-18 06:58:37');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `picture` varchar(255) NOT NULL,
  `note` text,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `id_category`, `id_user`, `name`, `slug`, `price`, `picture`, `note`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(7, 1, 1, 'nama kaos', 'nama-kaos-7', 90000, '/fileman/Uploads/baju/koko/k1.jpg', '<p>hello</p>', '0', NULL, '2015-09-15 18:22:53', '2015-09-15 18:37:26'),
(8, 1, 1, 'nama kaos', 'nama-kaos-8', 60000, '/fileman/Uploads/baju/koko/k1.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:32:45', '2015-09-15 18:32:45'),
(9, 1, 1, 'nama kaos', 'nama-kaos-9', 90000, '/fileman/Uploads/baju/koko/k10.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:33:17', '2015-09-15 18:33:17'),
(10, 1, 1, 'nama kaos', 'nama-kaos-10', 80000, '/fileman/Uploads/baju/koko/k2.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:33:35', '2015-09-15 18:33:35'),
(11, 1, 1, 'nama kaos', 'nama-kaos-11', 70000, '/fileman/Uploads/baju/koko/k2.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:37:17', '2015-09-15 18:37:17'),
(12, 1, 1, 'nama kaos', 'nama-kaos-12', 90000, '/fileman/Uploads/baju/koko/k3.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:37:46', '2015-09-15 18:37:46'),
(13, 1, 1, 'nama kaos', 'nama-kaos-13', 60000, '/fileman/Uploads/baju/koko/k3.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:37:59', '2015-09-15 18:37:59'),
(14, 1, 1, 'nama kaos', 'nama-kaos-14', 90000, '/fileman/Uploads/baju/koko/k4.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:38:27', '2015-09-15 18:38:27'),
(15, 1, 1, 'nama kaos', 'nama-kaos-15', 90000, '/fileman/Uploads/baju/koko/k5.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:38:42', '2015-09-15 18:38:42'),
(16, 1, 1, 'nama kaos', 'nama-kaos-16', 90000, '/fileman/Uploads/baju/koko/k5.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:39:00', '2015-09-15 18:39:00'),
(17, 1, 1, 'nama kaos', 'nama-kaos-17', 120000, '/fileman/Uploads/baju/koko/k6.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:39:22', '2015-09-15 18:39:22'),
(18, 1, 1, 'nama kaos', 'nama-kaos-18', 130000, '/fileman/Uploads/baju/koko/k6.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:39:40', '2015-09-15 18:39:40'),
(19, 1, 1, 'nama kaos', 'nama-kaos-19', 110000, '/fileman/Uploads/baju/koko/k7.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:40:23', '2015-09-15 18:40:23'),
(20, 1, 1, 'nama kaos', 'nama-kaos-20', 300000, '/fileman/Uploads/baju/koko/k7.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:40:40', '2015-09-15 18:40:40'),
(21, 1, 1, 'nama kaos', 'nama-kaos-21', 90000, '/fileman/Uploads/baju/koko/k8.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:41:01', '2015-09-15 18:41:01'),
(22, 2, 1, 'nama hijab anak', 'nama-hijab-anak-22', 80000, '/fileman/Uploads/baju/anak/a1.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:42:03', '2015-09-15 18:42:03'),
(23, 2, 1, 'nama hijab anak', 'nama-hijab-anak-23', 90000, '/fileman/Uploads/baju/anak/a10.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:42:24', '2015-09-15 18:42:24'),
(24, 2, 1, 'nama hijab anak', 'nama-hijab-anak-24', 90000, '/fileman/Uploads/baju/anak/a2.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:42:38', '2015-09-15 18:42:38'),
(25, 2, 1, 'nama hijab anak', 'nama-hijab-anak-25', 90000, '/fileman/Uploads/baju/anak/a3.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:43:03', '2015-09-15 18:43:03'),
(26, 2, 1, 'nama hijab anak', 'nama-hijab-anak-26', 90000, '/fileman/Uploads/baju/anak/a4.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:43:16', '2015-09-15 18:43:16'),
(27, 2, 1, 'nama hijab anak', 'nama-hijab-anak-27', 90000, '/fileman/Uploads/baju/anak/a5.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:43:29', '2015-09-15 18:43:29'),
(28, 2, 1, 'nama hijab anak', 'nama-hijab-anak-28', 90000, '/fileman/Uploads/baju/anak/a6.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:43:41', '2015-09-15 18:43:41'),
(29, 2, 1, 'nama hijab anak', 'nama-hijab-anak-29', 90000, '/fileman/Uploads/baju/anak/a7.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:44:04', '2015-09-15 18:44:04'),
(30, 2, 1, 'nama hijab anak', 'nama-hijab-anak-30', 90000, '/fileman/Uploads/baju/anak/a8.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:44:15', '2015-09-15 18:44:15'),
(31, 2, 1, 'nama hijab anak', 'nama-hijab-anak-31', 90000, '/fileman/Uploads/baju/anak/a9.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:44:33', '2015-09-15 18:44:33'),
(32, 3, 1, 'nama gamis', 'nama-gamis-32', 90000, '/fileman/Uploads/baju/gamis/g2.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:45:51', '2015-09-15 18:45:51'),
(33, 3, 1, 'nama gamis', 'nama-gamis-33', 90000, '/fileman/Uploads/baju/gamis/g3.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:46:04', '2015-09-15 18:46:04'),
(34, 3, 1, 'nama gamis', 'nama-gamis-34', 90000, '/fileman/Uploads/baju/gamis/g4.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:46:18', '2015-09-15 18:46:18'),
(35, 3, 1, 'nama gamis', 'nama-gamis-35', 90000, '/fileman/Uploads/baju/gamis/g5.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:46:31', '2015-09-15 18:46:31'),
(36, 3, 1, 'nama gamis', 'nama-gamis-36', 90000, '/fileman/Uploads/baju/gamis/g6.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:46:44', '2015-09-15 18:46:44'),
(37, 3, 1, 'nama gamis', 'nama-gamis-37', 90000, '/fileman/Uploads/baju/gamis/g7.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:46:55', '2015-09-15 18:46:55'),
(38, 3, 1, 'nama gamis', 'nama-gamis-38', 90000, '/fileman/Uploads/baju/gamis/g8.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:47:08', '2015-09-15 18:47:08'),
(39, 3, 1, 'nama gamis', 'nama-gamis-39', 90000, '/fileman/Uploads/baju/gamis/g9.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:47:21', '2015-09-15 18:47:21'),
(40, 4, 1, 'nama kaos', 'nama-kaos-40', 90000, '/fileman/Uploads/baju/kaos/baju1.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:48:20', '2015-09-15 18:48:20'),
(41, 4, 1, 'nama kaos', 'nama-kaos-41', 90000, '/fileman/Uploads/baju/kaos/baju2.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:48:34', '2015-09-15 19:11:46'),
(42, 4, 1, 'nama kaos', 'nama-kaos-42', 90000, '/fileman/Uploads/baju/kaos/baju3.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:48:59', '2015-09-15 19:11:10'),
(43, 4, 1, 'nama kaos', 'nama-kaos-43', 90000, '/fileman/Uploads/baju/kaos/baju4.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:49:11', '2015-09-15 19:11:33'),
(44, 4, 1, 'nama kaos', 'nama-kaos-44', 90000, '/fileman/Uploads/baju/kaos/baju5.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:49:25', '2015-09-15 18:49:25'),
(45, 4, 1, 'nama kaos', 'nama-kaos-45', 90000, '/fileman/Uploads/baju/kaos/baju6.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:49:37', '2015-09-15 19:11:25'),
(46, 4, 1, 'nama kaos', 'nama-kaos-46', 90000, '/fileman/Uploads/baju/kaos/baju7.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:50:09', '2015-09-15 19:11:20'),
(47, 4, 1, 'nama kaos', 'nama-kaos-47', 90000, '/fileman/Uploads/baju/kaos/baju8.jpg', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:50:21', '2015-09-15 19:11:16'),
(48, 5, 1, 'nama mukenah', 'nama-mukenah-48', 90000, '/fileman/Uploads/baju/mukenah/m1.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:50:49', '2015-09-15 18:50:49'),
(49, 5, 1, 'nama mukenah', 'nama-mukenah-49', 90000, '/fileman/Uploads/baju/mukenah/m10.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:51:01', '2015-09-15 19:11:04'),
(50, 5, 1, 'nama mukenah', 'nama-mukenah-50', 90000, '/fileman/Uploads/baju/mukenah/m2.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 18:51:17', '2015-09-15 19:09:55'),
(51, 5, 1, 'nama mukenah', 'nama-mukenah-51', 90000, '/fileman/Uploads/baju/mukenah/m8.png', '<p>bisa diisi deskripsi atau lain lain</p>', '0', NULL, '2015-09-15 19:10:42', '2015-09-15 19:10:42');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `prefix` varchar(10) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `listings` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `name`, `prefix`, `slug`, `id_user`, `status`, `listings`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Koko', 'DN', 'koko', 1, '1', 5, NULL, '2015-04-18 09:03:19', '2016-01-23 00:01:08'),
(2, 'Hijab Anak', 'photo', 'hijab-anak', 1, '1', 4, NULL, '2015-04-25 00:45:59', '2016-01-23 00:01:08'),
(3, 'Gamis', '', 'gamis', 1, '1', 3, NULL, '2015-08-14 10:00:00', '2016-01-23 00:01:08'),
(4, 'kaos', '', 'kaos', 1, '0', 2, NULL, '2015-09-15 17:25:30', '2016-01-23 00:01:08'),
(5, 'mukenah', '', 'mukenah', 1, '0', 1, NULL, '2015-09-15 17:33:23', '2016-01-23 00:01:08');

-- --------------------------------------------------------

--
-- Table structure for table `product_visitors`
--

CREATE TABLE `product_visitors` (
  `id` int(11) NOT NULL,
  `id_products` int(11) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `testimony`
--

CREATE TABLE `testimony` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimony`
--

INSERT INTO `testimony` (`id`, `picture`, `status`, `created_at`, `updated_at`) VALUES
(5, '/fileman/Uploads/dreamland_beach_13603_1920x1200_ - Copy 1.jpg', '1', '2015-04-07 07:49:29', '2015-04-07 07:49:39'),
(6, '/fileman/Uploads/bri.gif', '1', '2015-04-07 07:50:05', '2015-04-07 07:50:27'),
(7, '/fileman/Uploads/bni.png', '1', '2015-04-07 07:51:29', '2015-04-07 07:51:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `bbm` varchar(20) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `address`, `phone`, `bbm`, `facebook`, `twitter`, `instagram`, `is_active`, `level`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Nuli Giarsyani', 'admin@example.com', 'd58b87ef91477cdee2ccb45b2c49bc69', 'sdada', '081915804771', '', 'http://facebook.com/giarsyaninuli', 'http://twitter.com/giarsyaninuli', 'http://instagram.com/giarsyaninuli', '1', 1, NULL, '2014-10-24 19:58:52', '2015-03-24 10:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `group_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2015-09-15 17:59:07', '2015-03-24 03:24:36'),
(2, 'Operator', '2015-09-15 17:59:07', '2015-03-24 03:24:31');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(32) NOT NULL,
  `id_product` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`id`, `ip`, `id_product`, `created_at`, `updated_at`) VALUES
(4, '127.0.0.1', 40, '2015-09-15 18:57:22', '2015-09-15 18:57:22'),
(5, '127.0.0.1', 48, '2015-09-15 18:58:54', '2015-09-15 18:58:54'),
(6, '127.0.0.1', 48, '2015-09-15 18:59:32', '2015-09-15 18:59:32'),
(7, '127.0.0.1', 48, '2015-09-15 18:59:48', '2015-09-15 18:59:48'),
(8, '127.0.0.1', 46, '2015-09-15 19:24:37', '2015-09-15 19:24:37'),
(9, '127.0.0.1', 31, '2015-09-15 19:24:46', '2015-09-15 19:24:46'),
(10, '127.0.0.1', 51, '2015-09-15 21:24:27', '2015-09-15 21:24:27'),
(11, '127.0.0.1', 51, '2015-09-15 21:25:29', '2015-09-15 21:25:29'),
(12, '127.0.0.1', 48, '2016-01-23 00:03:13', '2016-01-23 00:03:13'),
(13, '127.0.0.1', 49, '2016-01-23 00:03:25', '2016-01-23 00:03:25'),
(14, '127.0.0.1', 46, '2016-04-06 23:37:08', '2016-04-06 23:37:08'),
(15, '127.0.0.1', 46, '2016-04-06 23:37:20', '2016-04-06 23:37:20'),
(16, '127.0.0.1', 39, '2016-04-12 08:31:50', '2016-04-12 08:31:50'),
(17, '127.0.0.1', 31, '2016-04-12 08:32:00', '2016-04-12 08:32:00'),
(18, '127.0.0.1', 29, '2016-04-12 08:32:04', '2016-04-12 08:32:04'),
(19, '127.0.0.1', 27, '2016-04-12 08:32:09', '2016-04-12 08:32:09'),
(20, '127.0.0.1', 23, '2016-04-12 08:32:13', '2016-04-12 08:32:13'),
(21, '127.0.0.1', 46, '2016-04-12 08:32:18', '2016-04-12 08:32:18'),
(22, '127.0.0.1', 37, '2016-04-12 08:32:21', '2016-04-12 08:32:21'),
(23, '127.0.0.1', 20, '2016-04-12 08:32:26', '2016-04-12 08:32:26'),
(24, '127.0.0.1', 21, '2020-06-24 09:55:53', '2020-06-24 09:55:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`),
  ADD KEY `id_controller` (`id_controller`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_parent` (`id_parent`);

--
-- Indexes for table `how_to_buy`
--
ALTER TABLE `how_to_buy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_blogs_1` (`id_user`),
  ADD KEY `fk_blogs_2` (`id_category`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_products_1` (`id_category`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_visitors`
--
ALTER TABLE `product_visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimony`
--
ALTER TABLE `testimony`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `controllers`
--
ALTER TABLE `controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `testimony`
--
ALTER TABLE `testimony`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
